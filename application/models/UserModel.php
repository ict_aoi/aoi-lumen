<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

    
    public function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->db1 = $CI->load->database('default',TRUE);
        
        
    }
    

    function allposts_count_regis()
    {
        $nik = $this->session->userdata('nik');
        $query = $this->db2->query("SELECT * from registrasi_view");
        return $query->num_rows();

    }  
 
      
    
    
    function allposts_regis($limit,$start,$col,$dir)
    {
        $nik = $this->session->userdata('nik');
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            
            $query = $this->db2->query("SELECT * from registrasi_view 
            order by $col $dir 
            limit $limit offset $start");
        }
        else {

            $query = $this->db2->query("SELECT * from registrasi_view ");
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
        }
   
    function posts_search_regis($limit,$start,$search,$col,$dir)
    {
        $nik = $this->session->userdata('nik');
  
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $text = "";

            $text .= "SELECT * from registrasi_view";
            
            if($search!=NULL){
                $text .= " where LOWER(name) like '%$search%'           

                order by $col $dir limit $limit offset $start";
            }
        }
        else{
            $text = "";

            $text .= "SELECT * from registrasi_view";

            if($search1!=NULL){
				$text .= " where LOWER(name) like '%$search%'";
            }
        }

        $query = $this->db2->query($text);
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
       
    
    }

    function posts_search_count_regis($search)
    {
        $nik = $this->session->userdata('nik');
        $text = "";

        $text .= "SELECT * from registrasi_view";

        if($search!=NULL){
            $text .= " where LOWER(name) like '%$search%'";
        }

        $query = $this->db2->query($text);
    
        return $query->num_rows();
    }


    public function tambah_peserta($data, $table)
    {
    
        $this->db2->insert($table,$data);
    }

    function allposts_count_pertemuan()
    {
        $nik = $this->session->userdata('nik');
        $query = $this->db2->query("SELECT * from pertemuan_view where nik ='$nik' and delete_date is null and delete_by is null");
        return $query->num_rows();

    }  
 
      
    
    
    function allposts_pertemuan($limit,$start,$col,$dir)
    {
        $nik = $this->session->userdata('nik');
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            
            $query = $this->db2->query("SELECT * from pertemuan_view where nik ='$nik' and delete_date is null and delete_by is null
            order by $col $dir 
            limit $limit offset $start");
        }
        else {

            $query = $this->db2->query("SELECT * from pertemuan_view where nik ='$nik' and delete_date is null and delete_by is null");
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
        }
   
    function posts_search_pertemuan($limit,$start,$search,$col,$dir)
    {
        $nik = $this->session->userdata('nik');
  
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $text = "";

            $text .= "SELECT * from pertemuan_view where nik ='$nik' and delete_date is null and delete_by is null";
            
            if($search!=NULL){
                $text .= " and judul like '%$search%'           

                order by $col $dir limit $limit offset $start";
            }
        }
        else{
            $text = "";

            $text .= "SELECT * from pertemuan_view where nik ='$nik' and delete_date is null and delete_by is null";

            if($search1!=NULL){
				$text .= " and judul like '%$search%'";
            }
        }

        $query = $this->db2->query($text);
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
       
    
    }

    function posts_search_count_pertemuan($search)
    {
        $nik = $this->session->userdata('nik');
        $text = "";

        $text .= "SELECT * from pertemuan_view where nik ='$nik' and delete_date is null and delete_by is null";

        if($search!=NULL){
            $text .= " and judul like '%$search%'";
        }

        $query = $this->db2->query($text);
    
        return $query->num_rows();
    }

    function getdata_head($department_name,$factory)
    {
        $sql = "SELECT nik, name FROM adt_bbigroup_all_emp_new WHERE (position NOT LIKE '%Sub%' AND position LIKE '%Head%') or  (position NOT LIKE '%SUB%' AND position LIKE '%HEAD%') or nik ='120202129' ";
        $query = $this->db1->query($sql);

        return $query;
    }
    function getdata_subdept($department_name,$factory)
    {
        $sql = "SELECT nik, name FROM adt_bbigroup_all_emp_new WHERE position LIKE '%Sub Dept%' or position like '%SUB DEPT%'";

        $query = $this->db1->query($sql);

        return $query;
    }
    function getdata_spv($department_name, $factory)
    {
        $sql = "SELECT nik, name FROM adt_bbigroup_all_emp_new WHERE position LIKE '%Super%' or position like '%SUPER%'";
        $query = $this->db1->query($sql);

        return $query;
    }
    public function tambah_pertemuan($data, $table)
    {
    
        $this->db2->insert($table,$data);
    }

    function DetailPertemuanSS($id_pertemuan)
    {
        $cek = $this->db2->query("SELECT nik_spv, nik_subdept_head, nik_dept_head from pertemuan_improvement_ss where id_pertemuan  = '$id_pertemuan'")->row_array();
        $sql = "";
    
        $sql .= "SELECT * FROM detail_ss where id_pertemuan = '$id_pertemuan'";
        if($cek['nik_spv'] != null){
            $sql .="and spv_status = '1'";
        }
        if($cek['nik_subdept_head'] != null){
            $sql .="and subdept_status = '1'";
        }
        if($cek['nik_dept_head'] != null){
            $sql .="and depthead_status = '1'";
        }
        $query = $this->db2->query($sql)->result();
        return $query;
    }

    function DetailNilaiSS_spv($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_spv=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_subdept_head($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_subdept_head=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_dept_head($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_dept_head=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_komite($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_komite=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }

    function upload(){
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';
        $config['remove_space'] = TRUE;
      
        $this->load->library('upload', $config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('produk_bef')){ // Lakukan upload dan Cek jika proses upload berhasil
          // Jika berhasil :
          $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
          return $return;
        }else{
          // Jika gagal :
          $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
          return $return;
        }
    }
    function tambah_laporan_ss($data, $table)
        {   
              $this->db2->insert($table, $data); 
        }

    function update_laporan_ss($data, $table, $id)
    {
        $this->db2->set($data);
        $this->db2->where('id_laporan', $id);
        $this->db2->update($table);
    }
 

   

   

    

}

/* End of file Peserta_model.php */
