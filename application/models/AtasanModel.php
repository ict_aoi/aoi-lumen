<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AtasanModel extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->db1 = $CI->load->database('default',TRUE);
    }



    function allposts_count_permintaan()
    {
        
        $nik = $this->session->userdata('nik');

        $sql1   = "SELECT approve_spv FROM pertemuan_improvement_ss WHERE nik_spv = '$nik'";
        $query1 = $this->db2->query($sql1)->row();
        
        $sql2   = "SELECT approve_subdept_head FROM pertemuan_improvement_ss WHERE nik_subdept_head = '$nik' ";
        $query2 = $this->db2->query($sql2)->row();
        $sql3   = "SELECT approve_dept_head FROM pertemuan_improvement_ss WHERE nik_dept_head = '$nik' ";
        $query3 = $this->db2->query($sql3)->row();
        
        if($query1 != NULL){
            $query = $this->db2->query("SELECT * from pertemuan_view
                        where nik_spv = '$nik' and delete_date is null and delete_by is null");
            return $query->num_rows();
        } else if($query2 != NULL){
            $query = $this->db2->query("SELECT * from pertemuan_view
                        where nik_subdept_head = '$nik' and delete_date is null and delete_by is null");
            return $query->num_rows();
           
        } else if($query3 != NULL){
            $query = $this->db2->query("SELECT * from pertemuan_view
                        where nik_dept_head = '$nik' and delete_date is null and delete_by is null");
            return $query->num_rows();
           
        } else{
            
        }
    }  
 
    function allposts_permintaan($limit,$start,$col,$dir)
    {
        $nik = $this->session->userdata('nik');
        $sql1   = "SELECT approve_spv FROM pertemuan_improvement_ss WHERE nik_spv = '$nik'";
        $query1 = $this->db2->query($sql1)->row();
        
        $sql2   = "SELECT approve_subdept_head FROM pertemuan_improvement_ss WHERE nik_subdept_head = '$nik' ";
        $query2 = $this->db2->query($sql2)->row();
        $sql3   = "SELECT approve_dept_head FROM pertemuan_improvement_ss WHERE nik_dept_head = '$nik' ";
        $query3 = $this->db2->query($sql3)->row();
        
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            if($query1 != NULL){
                $query = $this->db2->query("SELECT * from pertemuan_view
					where nik_spv = '$nik' and delete_date is null and delete_by is null
					order by $col $dir , approve_spv asc , date_created desc
					limit $limit offset $start");
					if($query->num_rows()>0)
					{
						return $query->result(); 
					}
					else
					{
						return null;
					}
                            
                            
               
            } else if($query2 != NULL){
                $query = $this->db2->query("SELECT * from pertemuan_view
                            where nik_subdept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 
                            order by $col $dir ,approve_subdept_head asc , date_created desc
                            limit $limit offset $start");
                            if($query->num_rows()>0)
                            {
                                return $query->result(); 
                            }
                            else
                            {
                                return null;
                            }
                            
                            
             
            } else if($query3 != NULL){
                $query = $this->db2->query("SELECT * from pertemuan_view
                            where nik_dept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 and approve_subdept_head != 2
                            order by $col $dir, approve_dept_head asc , date_created desc
                            limit $limit offset $start");
                            if($query->num_rows()>0)
                            {
                                return $query->result(); 
                            }
                            else
                            {
                                return null;
                            }
                            
                            
               
            } else{
                
            }
         
        }
        else {

            if($query1 != NULL){
                $query = $this->db2->query("SELECT * from pertemuan_view
                            where nik_spv = '$nik' and delete_date is null and delete_by is null 
							order by approve_spv asc , date_created desc");
                            if($query->num_rows()>0)
                            {
                                return $query->result(); 
                            }
                            else
                            {
                                return null;
                            }
                            
                            
              
            } else if($query2 != NULL){
                $query = $this->db2->query("SELECT * from pertemuan_view
                            where nik_subdept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 
							order by approve_subdept_head asc , date_created desc");
                            if($query->num_rows()>0)
                            {
                                return $query->result(); 
                            }
                            else
                            {
                                return null;
                            }
                            
                            
              
            } else if($query3 != NULL){
                $query = $this->db2->query("SELECT * from pertemuan_view
                            where nik_dept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 
							order by approve_dept_head asc , date_created desc");
                            if($query->num_rows()>0)
                            {
                                return $query->result(); 
                            }
                            else
                            {
                                return null;
                            }
                            
                            
              
            } else{
                
            }
            
        }

    } 
   
    function posts_search_permintaan($limit,$start,$search,$col,$dir,$dataFilter)
    {
        $nik = $this->session->userdata('nik');
        $sql1   = "SELECT approve_spv FROM pertemuan_improvement_ss WHERE nik_spv = '$nik'";
        $query1 = $this->db2->query($sql1)->row();
        
        $sql2   = "SELECT approve_subdept_head FROM pertemuan_improvement_ss WHERE nik_subdept_head = '$nik' ";
        $query2 = $this->db2->query($sql2)->row();
        $sql3   = "SELECT approve_dept_head FROM pertemuan_improvement_ss WHERE nik_dept_head = '$nik' ";
        $query3 = $this->db2->query($sql3)->row();
        
  
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $text = "";

            if($query1 != NULL){
                $text .="SELECT * from pertemuan_view
                            where nik_spv = '$nik' and delete_date is null and delete_by is null
							";

                    if($dataFilter[0] <> '' and $dataFilter[1]){
                        $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                    }
                    if($dataFilter[2] <> ""){
                        $text .= " and approve_spv = '$dataFilter[2]'";
                    }
                    $text .= " order by approve_spv asc , date_created desc";
               
            } else if($query2 != NULL){
                $text .="SELECT * from pertemuan_view
                            where nik_subdept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2
						";

                    if($dataFilter[0] <> '' and $dataFilter[1]){
                    $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                    }
                    if($dataFilter[2] <> ""){
                    $text .= " and approve_subdept_head = '$dataFilter[2]'";
                    }
                    $text .= " order by approve_subdept_head asc , date_created desc";
              
            } else if($query3 != NULL){
                $text .="SELECT * from pertemuan_view
                            where nik_dept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 and approve_subdept_head != 2
							";
                    if($dataFilter[0] <> '' and $dataFilter[1]){
                    $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                    }
                    if($dataFilter[2] <> ""){
                    $text .= " and approve_dept_head = '$dataFilter[2]'";
                    }
                    $text .= " order by approve_dept_head asc , date_created desc,  $col $dir limit $limit offset $start";
            
            } else{
                
            }
            
            // if($search!=NULL){
            //     $text .= " and name like '%$search%'           
			// 		order by $col $dir limit $limit offset $start";
            // }
        }
        else{
            $text = "";

            if($query1 != NULL){
                $text .="SELECT * from pertemuan_view
                            where nik_spv = '$nik' and delete_date is null and delete_by is null";
                 if($dataFilter[0] <> '' and $dataFilter[1]){
                    $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                }
                if($dataFilter[2] <> ""){
                    $text .= " and approve_spv = '$dataFilter[2]'";
                }
                $text .= "	order by approve_spv asc , date_created desc";
               
            } else if($query2 != NULL){
                $text .="SELECT * from pertemuan_view
                            where nik_subdept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2";
                if($dataFilter[0] <> '' and $dataFilter[1]){
                $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                }
                if($dataFilter[2] <> ""){
                $text .= " and approve_subdept_head = '$dataFilter[2]'";
                }

                $text .= "order by approve_subdept_head asc , date_created desc";
              
            } else if($query3 != NULL){
                $text .="SELECT * from pertemuan_view
                            where nik_dept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 and approve_subdept_head != 2";

                if($dataFilter[0] <> '' and $dataFilter[1]){
                $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                }
                if($dataFilter[2] <> ""){
                $text .= " and approve_dept_head = '$dataFilter[2]'";
                }

                $text .= "	order by approve_dept_head asc , date_created desc";

            } else{
                
            }

            // if($search!=NULL){
			// 	$text .= " and name like '%$search%'";
            // }
        }
        // var_dump($text);
        // die();
		
        $query = $this->db2->query($text);
        
		
        if($query->num_rows()>0)
        {
			return $query->result();  
        }
        else
        {
            return null;
        }
       
    
    }

    function posts_search_count_permintaan($search,$dataFilter)
    {
        $nik = $this->session->userdata('nik');
        $sql1   = "SELECT approve_spv FROM pertemuan_improvement_ss WHERE nik_spv = '$nik'";
        $query1 = $this->db2->query($sql1)->row();
        
        $sql2   = "SELECT approve_subdept_head FROM pertemuan_improvement_ss WHERE nik_subdept_head = '$nik'";
        $query2 = $this->db2->query($sql2)->row();
        $sql3   = "SELECT approve_dept_head FROM pertemuan_improvement_ss WHERE nik_dept_head = '$nik' ";
        $query3 = $this->db2->query($sql3)->row();
        $text = "";

        if($query1 != NULL){
            $text .="SELECT * from pertemuan_view
                        where nik_spv = '$nik' and delete_date is null and delete_by is null";
                if($dataFilter[0] <> '' and $dataFilter[1]){
                    $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                }
                if($dataFilter[2] <> ""){
                    $text .= " and approve_spv = '$dataFilter[2]'";
                }
           
        } else if($query2 != NULL){
            $text .="SELECT * from pertemuan_view
                        where nik_subdept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != 2 ";
                if($dataFilter[0] <> '' and $dataFilter[1]){
                $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                }
                if($dataFilter[2] <> ""){
                    $text .= " and approve_subdept_head = '$dataFilter[2]'";
                }
          
        } else if($query3 != NULL){
            $text .="SELECT * from pertemuan_view
                        where nik_dept_head = '$nik' and delete_date is null and delete_by is null and approve_spv != '2' and approve_subdept_head != '2'";
                if($dataFilter[0] <> '' and $dataFilter[1]){
                $text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
                 }
                if($dataFilter[2] <> ""){
                    $text .= " and approve_dept_head = '$dataFilter[2]'";
                }
            
        } else{
            
        }

        if($search!=NULL){
            $text .= " and name like '%$search%'";
        }

        $query = $this->db2->query($text);
    
        return $query->num_rows();
    }
  


  

    function DetailPertemuanSS($id_pertemuan)
    {
		
		// Yang lama
        // $cek = $this->db2->query("SELECT nik_spv, nik_subdept_head, nik_dept_head from pertemuan_improvement_ss where id_pertemuan  = '$id_pertemuan'")->row_array();
        // $sql = "";
    
        // $sql .= "SELECT * FROM detail_ss where id_pertemuan = '$id_pertemuan'";
        // if($cek['nik_spv'] != null){
        //     $sql .="and spv_status = '1'";
        // }
        // if($cek['nik_subdept_head'] != null){
        //     $sql .="and subdept_status = '1'";
        // }
        // if($cek['nik_dept_head'] != null){
        //     $sql .="and depthead_status = '1'";
        // }
        // $query = $this->db2->query($sql)->result();
		
		// Ambil MAX nya
		// $sql = "SELECT *  FROM detail_ss where id_pertemuan = '$id_pertemuan' and subdept_status = (select max(subdept_status) from detail_ss where id_pertemuan = '$id_pertemuan')";
        $sql = "SELECT *  FROM mna_detail_ss where id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();

		// var_dump($query);
		// die();

        return $query;
    }

    function DetailNilaiSS_spv($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_spv=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_subdept_head($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_subdept_head=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_dept_head($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_dept_head=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_komite($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_komite=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }

    function approve_permintaan_ss($id_pertemuan)
    {
        $nik = $this->session->userdata('nik');
        $sql1 = "SELECT nik FROM pertemuan_improvement_ss WHERE nik_spv = '$nik'";
        $query1 = $this->db2->query($sql1)->row();
        $sql2 = "SELECT nik FROM pertemuan_improvement_ss WHERE nik_subdept_head = '$nik'";
        $query2 = $this->db2->query($sql2)->row();
        $sql3 = "SELECT nik FROM pertemuan_improvement_ss WHERE nik_dept_head = '$nik'";
        $query3 = $this->db2->query($sql3)->row();

        // var_dump($query1);
        // die();
        if($query1 != NULL){
            $sql = "UPDATE pertemuan_improvement_ss SET approve_spv = 1 WHERE id_pertemuan = '$id_pertemuan'";
            $query = $this->db2->query($sql);
            return $query;
        } else if($query2 != NULL){
            $sql = "UPDATE pertemuan_improvement_ss SET approve_subdept_head = 1 WHERE id_pertemuan = '$id_pertemuan'";
            $query = $this->db2->query($sql);
            return $query;
        } else{
            $sql = "UPDATE pertemuan_improvement_ss SET approve_dept_head = 1 WHERE id_pertemuan = '$id_pertemuan'";
            $query = $this->db2->query($sql);
            return $query;
        }

      

    }
     function reject_permintaan_ss($id_pertemuan)
    {
        $nik = $this->session->userdata('nik');
        $sql1 = "SELECT nik FROM pertemuan_improvement_ss WHERE nik_spv = '$nik'";
        $query1 = $this->db2->query($sql1)->row();
        $sql2 = "SELECT nik FROM pertemuan_improvement_ss WHERE nik_subdept_head = '$nik'";
        $query2 = $this->db2->query($sql2)->row();
        $sql3 = "SELECT nik FROM pertemuan_improvement_ss WHERE nik_dept_head = '$nik'";
        $query3 = $this->db2->query($sql3)->row();

        

        if($query1 != NULL){
            $sql = "UPDATE pertemuan_improvement_ss SET approve_spv = 2 WHERE id_pertemuan = '$id_pertemuan'";
            $query = $this->db2->query($sql);
            return $query;
        } else if($query2 != NULL){
            $sql = "UPDATE pertemuan_improvement_ss SET approve_subdept_head =2 WHERE id_pertemuan = '$id_pertemuan'";
            $query = $this->db2->query($sql);
            return $query;
        } else{
            $sql = "UPDATE pertemuan_improvement_ss SET approve_dept_head = 2 WHERE id_pertemuan = '$id_pertemuan'";
            $query = $this->db2->query($sql);
            return $query;
        }


    }

    function simpan_nilai_ss($data, $table)
    {
        $this->db2->insert($table,$data);
    }
     function update_nilai_ss($data, $table, $id)
    {
        $this->db2->set($data);
        $this->db2->where('id_nilai', $id);
        $this->db2->update($table);
    
    }
    function data_pertemuan($id_pertemuan)
    {
        $sql = "SELECT * from pertemuan_improvement_ss JOIN
        (SELECT * from dblink('dbname=absensi_aoi port=5432 host=192.168.15.56 user=absensi password=Absensi12345',
         'SELECT nik,name ,department_name, subdept_name,position,factory FROM adt_bbigroup_all_emp_new') AS
         tb1(nik character varying(255),
             name character varying(255),
             department_name character varying(255), 
             subdept_name character varying(255),
             position character varying(255),
            factory character varying(255)) ) registrasi ON pertemuan_improvement_ss.nik=registrasi.nik
            where pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }

    function data_laporan_perbaikan($id_pertemuan)
    {
        $sql = "SELECT * FROM laporan_perbaikan_ss WHERE id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function data_nilai_ss_spv($id_pertemuan)
    {
        $sql = "SELECT * FROM nilai_improvement_ss 
        JOIN pertemuan_improvement_ss ON nilai_improvement_ss.id_pertemuan=pertemuan_improvement_ss.id_pertemuan
        WHERE pertemuan_improvement_ss.nik_spv = nilai_improvement_ss.nik and  nilai_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function data_nilai_ss_subdept($id_pertemuan)
    {
        $sql = "SELECT * FROM nilai_improvement_ss 
        JOIN pertemuan_improvement_ss ON nilai_improvement_ss.id_pertemuan=pertemuan_improvement_ss.id_pertemuan
        WHERE pertemuan_improvement_ss.nik_subdept_head = nilai_improvement_ss.nik  and nilai_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function data_nilai_ss_depthead($id_pertemuan)
    {
        $sql = "SELECT * FROM nilai_improvement_ss 
        JOIN pertemuan_improvement_ss ON nilai_improvement_ss.id_pertemuan=pertemuan_improvement_ss.id_pertemuan
        WHERE pertemuan_improvement_ss.nik_dept_head = nilai_improvement_ss.nik and nilai_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function data_nilai_ss_komite($id_pertemuan)
    {
        $sql = "SELECT * FROM nilai_improvement_ss 
        JOIN pertemuan_improvement_ss ON nilai_improvement_ss.id_pertemuan=pertemuan_improvement_ss.id_pertemuan
        WHERE pertemuan_improvement_ss.nik_komite = nilai_improvement_ss.nik and nilai_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function getnama_spv($id_pertemuan)
    {
        $sql = "SELECT * from pertemuan_improvement_ss JOIN
        (SELECT * from dblink('dbname=absensi_aoi port=5432 host=192.168.15.56 user=absensi password=Absensi12345',
         'SELECT nik,name FROM adt_bbigroup_all_emp_new') AS
         tb1(nik character varying(255),
             name character varying(255)) ) registrasi ON pertemuan_improvement_ss.nik_spv=registrasi.nik
            where pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function getnama_subdept($id_pertemuan)
    {
        $sql = "SELECT * from pertemuan_improvement_ss JOIN
        (SELECT * from dblink('dbname=absensi_aoi port=5432 host=192.168.15.56 user=absensi password=Absensi12345',
         'SELECT nik,name FROM adt_bbigroup_all_emp_new') AS
         tb1(nik character varying(255),
             name character varying(255)) ) registrasi ON pertemuan_improvement_ss.nik_subdept_head=registrasi.nik
            where pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function getnama_dept($id_pertemuan)
    {
        $sql = "SELECT * from pertemuan_improvement_ss JOIN
        (SELECT * from dblink('dbname=absensi_aoi port=5432 host=192.168.15.56 user=absensi password=Absensi12345',
         'SELECT nik,name FROM adt_bbigroup_all_emp_new') AS
         tb1(nik character varying(255),
             name character varying(255)) ) registrasi ON pertemuan_improvement_ss.nik_dept_head=registrasi.nik
            where pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function getnama_komite($id_pertemuan)
    {
        $sql = "SELECT * from pertemuan_improvement_ss JOIN
        (SELECT * from dblink('dbname=absensi_aoi port=5432 host=192.168.15.56 user=absensi password=Absensi12345',
         'SELECT nik,name FROM adt_bbigroup_all_emp_new') AS
         tb1(nik character varying(255),
             name character varying(255)) ) registrasi ON pertemuan_improvement_ss.nik_komite=registrasi.nik
            where pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        return $query;
    }

  


    
    
    

}

/* End of file AtasanModel.php */
