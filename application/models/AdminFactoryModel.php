<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminFactoryModel extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->db1 = $CI->load->database('default',TRUE);
        
        // date_default_timezone_set('Asia/Jakarta');
        
    }
    function allposts_count_pertemuan()
    {
        $factory_id = $this->session->userdata('factory_id');
        if($factory_id == "1")
        {
            $factory_id = "AOI1";
        }else if($factory_id == "2")
        {
            $factory_id = "AOI2";
        }
        $query = $this->db2->query("SELECT * from pertemuan_view
                    where factory ='$factory_id' and delete_date is null and delete_by is null ORDER BY date_created desc");
        return $query->num_rows();

    }  
 
    function allposts_pertemuan($limit,$start,$col,$dir)
    {
        $factory_id = $this->session->userdata('factory_id');
        if($factory_id == "1")
        {
            $factory_id = "AOI1";
        }else if($factory_id == "2")
        {
            $factory_id = "AOI2";
        }

        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            
            $query = $this->db2->query("SELECT * from pertemuan_view
                        where factory ='$factory_id' and delete_date is null and delete_by is null
            order by $col $dir , meeting_date desc
            limit $limit offset $start");
        }
        else {

            $query = $this->db2->query("SELECT * from pertemuan_view
                        where factory ='$factory_id' and delete_date is null and delete_by is null ORDER BY meeting_date desc limit $limit offset $start");
        }

        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
        }
   
    function posts_search_pertemuan($limit,$start,$search, $col,$dir, $dataFilter)
    {
        $factory_id = $this->session->userdata('factory_id');
        if($factory_id == "1")
        {
            $factory_id = "AOI1";
        }else if($factory_id == "2")
        {
            $factory_id = "AOI2";
        }
  
        if($limit!=NULL && $start!=NULL && $col!=NULL && $dir!=NULL){
            $text = "";

            $text .= "SELECT distinct(id_pertemuan), * from pertemuan_view
                        where factory ='$factory_id' ";
				if($dataFilter[0] <> '' and $dataFilter[1]){
					$text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
				}
				if($dataFilter[2] <> ""){
					$text .= " and factory = '$dataFilter[2]'";
				}
				if($dataFilter[3] <> ""){
					$dept = trim($dataFilter[3], ' ');
				    $text .= " and trim(department_name) = '$dept'";
				}
				
				$text .= " and delete_date is null and delete_by is null order by name desc limit $limit offset $start";
				// $text .= " and delete_date is null and delete_by is null order by name asc limit $limit offset $start";

        }
        else{
            $text = "";

            $text .= "SELECT distinct(id_pertemuan), * from pertemuan_view
                        where factory ='$factory_id'";
			if($dataFilter[0] <> '' and $dataFilter[1]){
				$text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
			}
			if($dataFilter[2] <> ""){
				$text .= " and factory = '$dataFilter[2]'";
			}
			if($dataFilter[3] <> ""){
				$dept = trim($dataFilter[3], ' ');
				$text .= " and trim(department_name) = '$dept'";
			}
			$text .= " and delete_date is null and delete_by is null order by name asc limit $limit offset $start";
        }

		// $text = "SELECT distinct(id_pertemuan), * from pertemuan_view where nik_komite ='$nik' and factory = '$dataFilter[1]'";

        $query = $this->db2->query($text);

		// var_dump($text);
		// die();
        
       
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
       
    
    }

    function posts_search_count_pertemuan($search, $dataFilter)
    {
        $factory_id = $this->session->userdata('factory_id');
        if($factory_id == "1")
        {
            $factory_id = "AOI1";
        }else if($factory_id == "2")
        {
            $factory_id = "AOI2";
        }
        $text = "";

        $text .= "SELECT distinct(id_pertemuan), * from pertemuan_view
                    where factory ='$factory_id' and delete_date is null and delete_by is null";

		if($dataFilter[0] <> '' and $dataFilter[1]){
			$text .= " and meeting_date between '$dataFilter[0]' and '$dataFilter[1]'";
		}
		if($dataFilter[2] <> ""){
			$text .= " and factory = '$dataFilter[2]'";
		}
		if($dataFilter[3] <> ""){
			$dept = trim($dataFilter[3], ' ');
			$text .= " and trim(department_name) = '$dept'";
		}

        $query = $this->db2->query($text);
    
        return $query->num_rows();
    }


    function DetailPertemuanSS($id_pertemuan)
    {
        // $cek = $this->db2->query("SELECT nik_spv, nik_subdept_head, nik_dept_head from pertemuan_improvement_ss where id_pertemuan  = '$id_pertemuan'")->row_array();
        $sql = "";
    
        $sql .= "SELECT * FROM detail_ss where id_pertemuan = '$id_pertemuan'";
        // if($cek['nik_spv'] != null){
        //     $sql .="and spv_status = '0'";
        // }
        // if($cek['nik_subdept_head'] != null){
        //     $sql .="and subdept_status = '1'";
        // }
        // if($cek['nik_dept_head'] != null){
        //     $sql .="and depthead_status = '1'";
        // }

        $query = $this->db2->query($sql)->result();
        return $query;
    }
    function DetailNilaiSS_spv($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_spv=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_subdept_head($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_subdept_head=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_dept_head($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_dept_head=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }
    function DetailNilaiSS_komite($id_pertemuan)
    {

        $sql = "SELECT * FROM pertemuan_improvement_ss
        LEFT JOIN nilai_improvement_ss on pertemuan_improvement_ss.id_pertemuan=nilai_improvement_ss.id_pertemuan and 
        pertemuan_improvement_ss.nik_komite=nilai_improvement_ss.nik WHERE pertemuan_improvement_ss.id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->result();
        
        return $query;  
    }

    

}

/* End of file ModelName.php */
