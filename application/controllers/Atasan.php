<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atasan extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AtasanModel');
        $this->load->model('UserModel');
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->db1 = $CI->load->database('default',TRUE);
        
        date_default_timezone_set('Asia/Jakarta');
        
    }
    

    public function index()
    {
        $this->load->view('ss/atasan/dashboard_permintaan');
        
    }
    public function list_permintaan()
    {
        $columns = array( 
            0 =>'name',
            1 =>'judul',
            2 =>'date_created',
            3 =>'meeting_date',
            4 =>'department_name',
            5 =>'factory',
            6 =>'approve_spv',
            7 =>'approve_subdept_head',
            8 =>'approve_dept_head',
            9 =>'approve_komite' 
            
        );
     //   $category = $this->input->post('category');
     
     
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');


        $totalData = $this->AtasanModel->allposts_count_permintaan();


        $totalFiltered = $totalData; 

        if(empty($this->input->post('search')['value']))
        {            
        $master = $this->AtasanModel->allposts_permintaan($limit,$start,$order,$dir);
        }
        else {
        $search = $this->input->post('search')['value']; 
        // explode buat misah 
				$dataFilter = explode('^', $search);
        // var_dump($dataFilter);
        // die();

        $master =  $this->AtasanModel->posts_search_permintaan($limit,$start,$search,$order,$dir,$dataFilter);
	
        $totalFiltered = $this->AtasanModel->posts_search_count_permintaan($search,$dataFilter);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
        foreach ($master as $key=>$master)
        {
        $_temp                  = '"'.$master->id_pertemuan.'"';
        if($master->nik_spv != null){
                          
            if($master->approve_spv == 0){
              $spv = "Belum di approve";
            } else if($master->approve_spv == 1){
              if($master->tgl_app_spv != null){
                $spv = "Approved at ". $master->tgl_app_spv;
              } else{
                $spv = "Approved ";
              }
               
            } else{
                $spv = "Ditolak";
            }    
          }else{
            $spv = "-";
          }
        if($master->nik_subdept_head != null){
                              
            if($master->approve_subdept_head == 0){
              $sub = "Belum di approve";
            } else if($master->approve_subdept_head == 1){
              if($master->tgl_app_sub != null){
                $sub = "Approved at ". $master->tgl_app_sub;
              } else{
                $sub = "Approved ";
              }
              
            } else{
                $sub = "Ditolak";
            } 
          } else{
            $sub = "-";
          } 

        if($master->nik_dept_head != null){
                              
            if($master->approve_dept_head == 0){
              $dept = "Belum di approve";
            } else if($master->approve_dept_head == 1){
              if($master->tgl_app_dept != null){
                $dept = "Approved at ". $master->tgl_app_dept;
              } else{
                $dept = "Approved ";
              }
               
            } else{
                $dept = "Ditolak";
            }
          }else{
            $dept = "-";
          }
        if($master->approve_komite == 0){
            $komite =  "Belum di approve";
        } else if($master->approve_komite == 1){
          if($master->tgl_app_kom != null){
            $komite = "Approved at ". $master->tgl_app_kom;
          } else{
            $komite = "Approved ";
          }
           
        } else{
            if($master->tgl_app_kom != null){
                $komite = "Ditolak at ". $master->tgl_app_kom;
              } else{
                $komite = "Ditolak";
              }
            
        } 

        $nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
        $nestedData['name']                 = ucwords($master->name);
        $nestedData['judul']                = ucwords($master->judul);
        $nestedData['date_created']         = ucwords($master->date_created);
        $nestedData['meeting_date']         = ucwords($master->meeting_date);
        $nestedData['department_name']      = ucwords($master->department_name);
        $nestedData['factory']              = ucwords($master->factory);
        $nestedData['approve_spv']          = ucwords($spv);
        $nestedData['approve_subdept_head'] = ucwords($sub);
        $nestedData['approve_dept_head']    = ucwords($dept);
        $nestedData['approve_komite']       = ucwords($komite);
        $nestedData['action']               = "<center><a href='".base_url()."Atasan/DetailPermintaanSS/$master->id_pertemuan' class='btn btn-xs btn-info'>Detail</a> </center>";
             
        $data[] = $nestedData;

        }
        }
    
      

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );
        
    
    
        echo json_encode($json_data);
    }

    public function DetailPermintaanSS($id_pertemuan)
    {
        

        $data['detail'] = $this->AtasanModel->DetailPertemuanSS($id_pertemuan);
        $data['nilai_spv'] = $this->AtasanModel->DetailNilaiSS_spv($id_pertemuan);
        $data['nilai_subdept'] = $this->AtasanModel->DetailNilaiSS_subdept_head($id_pertemuan);
        $data['nilai_dept'] = $this->AtasanModel->DetailNilaiSS_dept_head($id_pertemuan);
        $data['nilai_komite'] = $this->AtasanModel->DetailNilaiSS_komite($id_pertemuan);
        // var_dump($data);
        // die();
        $this->load->view('ss/atasan/detail_ss_permintaan', $data);
        
    }

    public function approve_permintaan_ss($id_pertemuan)
    {
				// var_dump($id_pertemuan);
				// die();

        $this->AtasanModel->approve_permintaan_ss($id_pertemuan);  

        $data = array(
            // 'id_approval' => rand(1,300),
            'id_pertemuan' => $id_pertemuan,
            'create_by' => $this->session->userdata('nik'),
            'create_date' => date('Y-m-d H:i:s'),
            'approval' => '1'
        );
        $this->db2->insert('approval', $data);
        $this->cek_permintaan_ss($id_pertemuan);
        redirect('Atasan/DetailPermintaanSS/'. $id_pertemuan);
        
    }
    public function reject_permintaan_ss($id_pertemuan)
    {
         $this->AtasanModel->reject_permintaan_ss($id_pertemuan);   
         $data = array(
            // 'id_approval' => rand(1,300),
            'id_pertemuan' => $id_pertemuan,
            'create_by' => $this->session->userdata('nik'),
            'create_date' =>  date('Y-m-d H:i:s'),
            'approval' => '2'
        );
        $this->db2->insert('approval', $data);
        $this->cek_permintaan_ss($id_pertemuan);
        redirect('Atasan/DetailPermintaanSS/'. $id_pertemuan);
    }

    // Validasi Approval Pertemuan
	public function cek_permintaan_ss($id_pertemuan)
	{
		$userNik = $this->session->userdata('nik');

		$sql = "SELECT * FROM approval  WHERE id_pertemuan = '$id_pertemuan' AND create_by = '$userNik'";
		$query = $this->db2->query($sql);

		// var_dump($query->result()[0]);
		// die();

		if ($query->num_rows() > 1) {
			for ($i = 0; $i < $query->num_rows(); $i++) {
				if ($i > 0) {
					$dataId = $query->result()[$i];
					$sql = "DELETE FROM approval  WHERE id = '$dataId->id'";
					$delQuery = $this->db2->query($sql);
				}
			}
		}
	}

    public function dashboard_formpenilaian($id_pertemuan)
    {
      
        $nik = $this->session->userdata('nik');
        $sql = "SELECT * FROM nilai_improvement_ss WHERE id_pertemuan = '$id_pertemuan' AND nik = '$nik'";
        $query = $this->db2->query($sql)->result();

       
        if($query != NULL){
            $data['cek'] = 1;
            $data['nilai'] = $query;
        } else{
            $data['cek'] = 2;
            $data['id_pertemuan'] = $id_pertemuan;
        }
        
        $this->load->view('ss/atasan/add_nilai', $data);   
    }

    public function simpan_nilai_ss($id_pertemuan)
    {
        $nik                 = $this->session->userdata('nik');
        $penghematan_biaya   = $this->input->post('penghematan_biaya');
        $penghematan_manhour = $this->input->post('penghematan_manhour');
        $safety              = $this->input->post('safety');
        $ergonomi            = $this->input->post('ergonomi');
        $quality             = $this->input->post('quality');
        $manfaat             = $this->input->post('manfaat');
        $keaslian            = $this->input->post('keaslian');
        $usaha               = $this->input->post('usaha');
        $komentar            = $this->input->post('komentar');
        $date = date('Y-m-d');

        // buat id nilai
        $id_nilai = 'NI'.$id_pertemuan.'_'.$nik;


       
        $sql = "SELECT id_nilai FROM nilai_improvement_ss WHERE nik = '$nik' AND id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->row();

        if($query != NULL){
            // total nilai
			//   if(is_numeric($penghematan_biaya) && is_numeric($penghematan_manhour) && is_numeric($safety)
			//   && is_numeric($ergonomi) && is_numeric($quality) && is_numeric($manfaat) && is_numeric($keaslian)
			//   && is_numeric($usaha)) : 
      $total_nilai = $penghematan_biaya + $penghematan_manhour + $safety  + $ergonomi + $quality
              + $manfaat + $keaslian  + $usaha;
       
              $get_id_nilai = $this->input->post('id_nilai');
              $data = array(
                  'id_nilai'            => $get_id_nilai,
                  'penghematan_biaya'   => $penghematan_biaya,
                  'penghematan_manhour' => $penghematan_manhour,
                  'safety'              => $safety,
                  'ergonomi'            => $ergonomi,
                  'quality'             => $quality,
                  'manfaat'             => $manfaat,
                  'keaslian'            => $keaslian,
                  'usaha'               => $usaha,
                  'total_nilai'         => $total_nilai,
                  'tanggal_penilaian'   => $date
              );
              $this->AtasanModel->update_nilai_ss($data, 'nilai_improvement_ss', $id_nilai);
              
              redirect('Atasan/DetailPermintaanSS/'.$id_pertemuan);   
			//   else :
			//       echo "-";
			//   endif;
        

      } else{
            // total nilai
			//   if(is_numeric($penghematan_biaya) && is_numeric($penghematan_manhour) && is_numeric($safety)
			//   && is_numeric($ergonomi) && is_numeric($quality) && is_numeric($manfaat) && is_numeric($keaslian)
			//   && is_numeric($usaha)) : 
				$total_nilai = $penghematan_biaya + $penghematan_manhour + $safety  + $ergonomi + $quality
								+ $manfaat + $keaslian  + $usaha;
              $data = array(
                  'id_nilai'            => $id_nilai,
                  'id_pertemuan'        => $id_pertemuan,
                  'nik'                 => $nik,
                  'penghematan_biaya'   => $penghematan_biaya,
                  'penghematan_manhour' => $penghematan_manhour,
                  'safety'              => $safety,
                  'ergonomi'            => $ergonomi,
                  'quality'             => $quality,
                  'manfaat'             => $manfaat,
                  'keaslian'            => $keaslian,
                  'usaha'               => $usaha,
                  'total_nilai'         => $total_nilai,
                  'tanggal_penilaian' => $date
              );
           
              $this->AtasanModel->simpan_nilai_ss($data, 'nilai_improvement_ss');
              
              redirect('Atasan/DetailPermintaanSS/'.$id_pertemuan);
			//   else :
			//       echo "-";
			//   endif;
        
          
      }
    }

    public function print($id_pertemuan)
    {
     
        $data['pertemuan'] = $this->AtasanModel->data_pertemuan($id_pertemuan);
        $data['laporan'] = $this->AtasanModel->data_laporan_perbaikan($id_pertemuan);
        $data['nilai_spv'] = $this->AtasanModel->data_nilai_ss_spv($id_pertemuan);
        $data['nilai_subdept'] = $this->AtasanModel->data_nilai_ss_subdept($id_pertemuan);
        $data['nilai_depthead'] = $this->AtasanModel->data_nilai_ss_depthead($id_pertemuan);
        $data['nilai_komite'] = $this->AtasanModel->data_nilai_ss_komite($id_pertemuan);
        $data['nama_spv'] = $this->AtasanModel->getnama_spv($id_pertemuan);
        $data['nama_subdept'] = $this->AtasanModel->getnama_subdept($id_pertemuan);
        $data['nama_dept'] = $this->AtasanModel->getnama_dept($id_pertemuan);
        $data['nama_komite'] = $this->AtasanModel->getnama_komite($id_pertemuan);
        $this->db2->where('id_pertemuan', $id_pertemuan);
        $this->db2->from('pertemuan_improvement_ss');
        $data['meeting_date'] = $this->db2->get()->row_array();
        $this->load->library('pdf');
        
        $this->pdf->setPaper('A4', 'vertikal');
        $this->pdf->filename = "Form.pdf";
        $this->pdf->load_view('ss/HasilPenilaian', $data); 
  
    }

    public function dashboard_list_regis()
    {
        $this->load->view('ss/atasan/dashboard_registrasi');
        
    }

    public function list_registrasi()
    {
        
        $columns = array( 
            0 => 'create_date',
            1 => 'nik',
            2 => 'name',
            3 => 'department_name',
            5 => 'position',
            6 => 'factory',
            7 => 'category'
            
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');


        $totalData = $this->UserModel->allposts_count_regis();


        $totalFiltered = $totalData; 

        if(empty($this->input->post('search')['value']))
        {            
        $master = $this->UserModel->allposts_regis($limit,$start,$order,$dir);
        }
        else {
        $search = $this->input->post('search')['value']; 

        $master =  $this->UserModel->posts_search_regis($limit,$start,$search,$order,$dir);

        $totalFiltered = $this->UserModels->posts_search_count_regis($search);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
        foreach ($master as $key=>$master)
        {
        $_temp                  = '"'.$master->id_registration_improvement.'"';

        $tgl_buat = date('d F Y', strtotime($master->create_date));
     

        $nestedData['no']              = (($draw-1) * 10) + (++$nomor_urut);
        $nestedData['create_date']     = ucwords($tgl_buat);
        $nestedData['nik']             = ucwords($master->nik);
        $nestedData['name']            = ucwords($master->name);
        $nestedData['department_name'] = ucwords($master->department_name);
        $nestedData['position']        = ucwords($master->position);
        $nestedData['factory']         = ucwords($master->factory);
        $nestedData['category']        = ucwords($master->category);
              
        $data[] = $nestedData;

        }
        }
    
      

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );
        
    
    
        echo json_encode($json_data);
     
    }

    public function dashboard_add_regis()
    {
        $this->load->view('ss/atasan/add_regis');
        
    }
    public function add_regis()
    {       
        $category = $this->input->post('optionsRadios');
        $tanggal = date("Y-m-d");
        $nik = $this->session->userdata('nik');
        $id = 'A'. $nik . rand(1,1000);

        $name = $this->input->post('name');
        $department_name = $this->input->post('department');
        $subdept_name = $this->input->post('subdepartment');
        $position = $this->input->post('position');
        $factory = $this->input->post('factory');
        $category = $this->input->post('optionsRadios');

        $data = array(
            'id_registration_improvement' => $id,
            'nik' =>  $nik,
            'category' => $category,
            'create_date' => $tanggal
          
        );

    
        $sql = "SELECT nik FROM pendaftaran_improvement WHERE nik = '$nik' AND category = '$category'";
        $query = $this->db2->query($sql)->row();

        $sql2= "SELECT nik FROM pendaftaran_improvement WHERE nik = '$nik' ";
        $query2 = $this->db2->query($sql2)->row();
        
        if($query2 != NULL){
            if($query != NULL){
                $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert"> Anda terdaftar di registrasi peserta di category ini <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect('Atasan/dashboard_add_regis');
            } else{
              //  $this->UserModel->tambah_peserta($data2,'user_data');
                $this->UserModel->tambah_peserta($data,'pendaftaran_improvement');
                
                redirect('Atasan/dashboard_add_regis');
            }
        } else{
         
            $this->UserModel->tambah_peserta($data,'pendaftaran_improvement');
            
            redirect('Atasan/dashboard_list_regis');
        }
      

        
        
    }

    function dashboard_list_pertemuan()
    {
        $this->load->view('ss/atasan/dashboard_pertemuan');
        
    }
    public function list_pertemuan()
    {
        
        $columns = array( 
            0 =>'name',
            1 =>'judul',
            2 =>'date_created',
            3 =>'meeting_date',
            4 =>'department_name',
            5 =>'factory',
            6 =>'approve_spv',
            7 =>'approve_subdept_head',
            8 =>'approve_dept_head',
            9 =>'approve_komite' 
            
        );
     
     
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');


        $totalData = $this->UserModel->allposts_count_pertemuan();


        $totalFiltered = $totalData; 

        if(empty($this->input->post('search')['value']))
        {            
        $master = $this->UserModel->allposts_pertemuan($limit,$start,$order,$dir);
        }
        else {
        $search = $this->input->post('search')['value']; 

        $master =  $this->UserModel->posts_search_pertemuan($limit,$start,$search,$order,$dir);

        $totalFiltered = $this->UserModel->posts_search_count_pertemuan($search);
        }

        $data = array();
        $nomor_urut = 0;
        if(!empty($master))
        {
        foreach ($master as $key=>$master)
        {
        $_temp                  = '"'.$master->id_pertemuan.'"';
        if($master->nik_spv != null){
                          
            if($master->approve_spv == 0){
              $spv = "Belum di approve";
            } else if($master->approve_spv == 1){
              if($master->tgl_app_spv != null && $master->tgl_app_spv == "1"){
                $spv = "Approved at ". $master->tgl_app_spv;
              } else{
                $spv = "Approved ";
              }
               
            } else{
                $spv = "Ditolak";
            }    
          }else{
            $spv = "-";
          }
        if($master->nik_subdept_head != null){
                              
            if($master->approve_subdept_head == 0){
              $sub = "Belum di approve";
            } else if($master->approve_subdept_head == 1){
              if($master->tgl_app_sub != null && $master->tgl_app_sub == "1"){
                $sub = "Approved at ". $master->tgl_app_sub;
              } else{
                $sub = "Approved ";
              }
              
            } else{
                $sub = "Ditolak";
            } 
          } else{
            $sub = "-";
          } 

        if($master->nik_dept_head != null){
                              
            if($master->approve_dept_head == 0){
              $dept = "Belum di approve";
            } else if($master->approve_dept_head == 1){
              if($master->tgl_app_dept != null && $master->tgl_app_dept == "1"){
                $dept = "Approved at ". $master->tgl_app_dept;
              } else{
                $dept = "Approved ";
              }
               
            } else{
                $dept = "Ditolak";
            }
          }else{
            $dept = "-";
          }
        if($master->approve_komite == 0){
            $komite =  "Belum di approve";
        } else if($master->approve_komite == 1){
          if($master->tgl_app_kom != null && $master->tgl_app_kom == "1"){
            $komite = "Approved at ". $master->tgl_app_kom;
          } else{
            $komite = "Approved ";
          }
           
        } else{
            $komite = "Ditolak";
        } 


        $nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
        $nestedData['name']                 = ucwords($master->name);
        $nestedData['judul']                = ucwords($master->judul);
        $nestedData['date_created']         = ucwords($master->date_created);
        $nestedData['meeting_date']         = ucwords($master->meeting_date);
        $nestedData['department_name']      = ucwords($master->department_name);
        $nestedData['factory']              = ucwords($master->factory);
        $nestedData['approve_spv']          = ucwords($spv);
        $nestedData['approve_subdept_head'] = ucwords($sub);
        $nestedData['approve_dept_head']    = ucwords($dept);
        $nestedData['approve_komite']       = ucwords($komite);
        $nestedData['action']               = "<center><a href='".base_url()."Atasan/DetailPertemuanSS/$master->id_pertemuan' class='btn btn-xs btn-info'>Detail</a> </center>";
             
        $data[] = $nestedData;

        }
        }
    
      

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );
        
    
    
        echo json_encode($json_data);
     
    }

    function dashboard_add_pertemuan()
    {
        $factory = $this->session->userdata('factory');
        $department_name = $this->session->userdata('department_name');
        $data['category'] = "SS"; 
        $this->load->view('ss/atasan/add_pertemuan', $data);
    }
    function data_subdept()
    {
        $json = [];
        if(!empty($this->input->get("subdept"))){
          $sub = $this->input->get("subdept");
          $query = $this->db1->query("SELECT nik,name, position from adt_bbigroup_emp_new WHERE (nik like '%$sub%' and position like '%Sub Dep%') 
          OR (nik like '%$sub%' and position like '%SUB DEP%') OR (nik like '%$sub%' and position like '%Presiden Direktur%')");
           $json = $query->result();
       }else{
        $this->db1->like('position', 'Sub Dept', 'both');
           $query = $this->db1->select('nik,name')
                       ->limit(10)
                       ->get("adt_bbigroup_emp_new");
        $json = $query->result();
       }
       echo json_encode($json);

    }


    function data_depthead()
    {

        $json = [];
        if(!empty($this->input->get("depthead"))){
          $dept = $this->input->get("depthead");
          $query = $this->db1->query("SELECT nik,name, position from adt_bbigroup_emp_new WHERE (nik like '%$dept%' and position like '%Head%' and position not like '%Sub%') 
          OR (nik like '%$dept%' and position like '%HEAD%' and position not like '%SUB%' ) or (nik like '%$dept%' and position ='Manager') ");
           $json = $query->result();
       }else{
        $query = $this->db1->query("SELECT nik,name, position from adt_bbigroup_emp_new WHERE (position like '%Head%' and position not like '%Sub%') 
        OR (position like '%HEAD%' and position not like '%SUB%' ) or (position ='Manager') ");
        $json = $query->result();
       }
       echo json_encode($json);
    //    var_dump($json);
    }
    public function tambah_pertemuan_ss()
    {
        
        $nik                  = $this->session->userdata('nik');
        $category_percategory = $this->input->post('category_percategory');
        $judul                = $this->input->post('judul');
        $uraian_masalah       = $this->input->post('uraian_masalah');
        $keadaan_seharusnya   = $this->input->post('keadaan_seharusnya');
        $usulan_perbaikan     = $this->input->post('usulan_perbaikan');
        $biaya_kerugian       = $this->input->post('biaya_kerugian');
        $location             = $this->input->post('location');
        // $nik_spv              = $this->input->post('spv');
        $nik_subdept          = $this->input->post('subdept');
        $nik_depthead         = $this->input->post('depthead');
        $date_created         = date("Y-m-d");
        $category             = $this->input->post('category');
        $get_cat              = implode(",",$category_percategory);
        $meeting_date         = $this->input->post('meeting_date');
        $meeting_time         = $this->input->post('meeting_time');
       
        $id_pertemuan_ss =  'SS' . $nik . rand(1,400);

        // $sql_getadmin = "SELECT nik FROM admin_data";
        // $query_getadmin = $this->db2->query($sql_getadmin)->row();
        $admin = "admin12";
     

        $sql = "SELECT id_registration_improvement FROM pendaftaran_improvement WHERE nik = '$nik' AND category = '$category'";
        $query = $this->db2->query($sql)->row();
        

        $query2 = $this->db2->query("SELECT id_pertemuan from pertemuan_improvement_ss  WHERE nik = '$nik'")->row();
        // var_dump($query2);
        // die();
     
        if($query != NULL){
            // if($query2 != NULL){

            //     $this->session->set_flashdata('notif','<div class="alert alert-info" role="alert"> Anda sudah membuat SS <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            //     redirect('Atasan/dashboard_add_pertemuan');
    
              
            // } else{
                
               
                $dept_name = $this->session->userdata('department_name');
                // $cekspasi = str_replace(" ", "_", $dept_name);
                // $ceksimbol = str_replace("/", "_", $cekspasi);
                $factory = $this->session->userdata('factory');
                $nomor = $this->db2->query("SELECT max(no_urut) as max FROM pertemuan_improvement_ss WHERE department_name = '$dept_name'
                and factory_id = '$factory'")->row_array();
                if($nomor != null){
                    $getno = (int)$nomor['max']+1;
                    // $id_pertemuan_ss =  $getno .'-'. $ceksimbol.'-'.$factory.'-'. date("Y");
                    $id_pertemuan_ss =  $getno .'-'.$factory.'-'. rand(1,1000);
                    $no_reg =  $getno .'-'. $dept_name.'-'.$factory.'-'. date("Y");
            
                } else{
                    $getno = '1';
                    // $id_pertemuan_ss =  $getno. '-'. $ceksimbol.'-'.$factory.'-'. date("Y");
                    $id_pertemuan_ss =  $getno .'-'.$factory.'-'. rand(1,1000);
                    $no_reg =  $getno .'-'. $dept_name.'-'.$factory.'-'. date("Y");
            
                }
                
                $data = array(
                    'id_pertemuan'                => $id_pertemuan_ss,
                    'nik'                         => $nik,
                    'category_percategory'        => $get_cat,
                    'judul'                       => $judul,
                    'keadaan_seharusnya'          => $keadaan_seharusnya,
                    'uraian_masalah'              => $uraian_masalah,
                    'usulan_perbaikan'            => $usulan_perbaikan,
                    'biaya_kerugian'              => $biaya_kerugian,
                    'location'                    => $location,
                    'nik_spv'                     => $nik_spv,
                    'nik_subdept_head'            => $nik_subdept,
                    'nik_dept_head'               => $nik_depthead,
                    'date_created'                => $date_created,
                    'meeting_date'                => $meeting_date,
                    'meeting_time'                => $meeting_time,
                    'id_registration_improvement' => $query->id_registration_improvement,
                    'approve_spv'                 => '0',
                    'approve_subdept_head'        => '0',
                    'approve_dept_head'           => '0',
                    'approve_komite'              => '0',
                    'nik_komite'                  => $admin,
                    'no_urut' => $getno,
                    'department_name' => $dept_name,
                    'factory_id'=> $factory,
                    'no_register' => $no_reg
                );
                
                $this->UserModel->tambah_pertemuan($data,'pertemuan_improvement_ss');
          
                redirect('Atasan/dashboard_list_pertemuan');
         //   }
            
        } else {
            $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert"> Anda belum registrasi di category ini <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('Atasan/dashboard_add_pertemuan');
    
        }



      
                
    }
    public function DetailPertemuanSS($id_pertemuan)
    {

        $data['detail'] = $this->UserModel->DetailPertemuanSS($id_pertemuan);
        $data['nilai_spv'] = $this->UserModel->DetailNilaiSS_spv($id_pertemuan);
        $data['nilai_subdept'] = $this->UserModel->DetailNilaiSS_subdept_head($id_pertemuan);
        $data['nilai_dept'] = $this->UserModel->DetailNilaiSS_dept_head($id_pertemuan);
        $data['nilai_komite'] = $this->UserModel->DetailNilaiSS_komite($id_pertemuan);

          $this->load->view('ss/atasan/detail_ss', $data);
        
    }

    function dashboard_formlaporan($id_pertemuan)
    {
        $nik = $this->session->userdata('nik');
        $sql = "SELECT * from laporan_perbaikan_ss WHERE id_pertemuan = '$id_pertemuan' and nik = '$nik'";
        $query = $this->db2->query($sql)->result();
        $data['laporan'] = $query;
        $data['id_pertemuan'] = $id_pertemuan;
        
        $this->load->view('ss/peserta/add_laporan_perbaikan', $data);
       

      
    }
   

    function tambah_laporan_ss($id_pertemuan)
    {

      $aktivitas_perbaikan = $this->input->post('aktivitas_perbaikan');
      $biaya_imp = $this->input->post('biaya_improvement');
      $perhitungan_benefit = $this->input->post('perhitungan_benefit');
      $quality_bef = $this->input->post('quality_before');
      $quality_aft = $this->input->post('quality_after');
      $cost_bef = $this->input->post('cost_before');
      $cost_aft = $this->input->post('cost_after');
      $delivery_bef = $this->input->post('delivery_before');
      $delivery_aft = $this->input->post('delivery_after');
      $safety_bef = $this->input->post('safety_before');
      $safety_aft = $this->input->post('safety_after');
      $moral_bef = $this->input->post('moral_before');
      $moral_aft = $this->input->post('moral_after');
      $productivity_bef = $this->input->post('productivity_before');
      $productivity_aft = $this->input->post('productivity_after');
      $env_bef = $this->input->post('environtment_before');
      $env_aft = $this->input->post('environtment_after');
      $id_laporan = 'LI'.$id_pertemuan;
      $nik = $this->session->userdata('nik');
  
        $config['max_size']      = 2048;
        $config['allowed_types'] = "png|jpg|jpeg|gif";
        $config['remove_spaces'] = TRUE;
        $config['overwrite']     = TRUE;
        $config['upload_path']   = FCPATH.'/upload';
        $config['file_name'] = $id_pertemuan.'_'.rand(1,100);


        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('produk_bef');
        $data_image=$this->upload->data('file_name');
        // $location=base_url().'upload/';
        // $pict=$location.$data_image;
        $pict=$data_image;
        $size1=$this->upload->data('file_size');
        $int_size1 = (int)$size1;


        $config2['max_size']      = 2048;
        $config2['allowed_types'] = "png|jpg|jpeg|gif";
        $config2['remove_spaces'] = TRUE;
        $config2['overwrite']     = TRUE;
        $config2['upload_path']   = FCPATH.'/upload';
        $config2['file_name'] = $id_pertemuan.'_'.rand(1,100);

        
        $this->load->library('upload');
        $this->upload->initialize($config2);
        $this->upload->do_upload('produk_aft');
        $data_image2=$this->upload->data('file_name');
        // $location=base_url().'upload/';
        $pict2=$data_image2;
        $size2=$this->upload->data('file_size');
        $int_size2 = (int)$size2;
    
 
    if($int_size1 > $config['max_size'] || $int_size2 > $config['max_size']){
        $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert"> Upload foto harus kurang dari 5mb <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('Atasan/dashboard_formlaporan/'.$id_pertemuan);
      
     } else{

      if($this->upload->do_upload('produk_aft') == false){
        $data = array(
          'produk_before'          =>$pict,
          'aktivitas_perbaikan' => $aktivitas_perbaikan,
          'biaya_imp'           => $biaya_imp,
          'perhitungan_benefit' => $perhitungan_benefit,
          'quality_before'      => $quality_bef,
          'quality_after'       => $quality_aft,
          'cost_before'         => $cost_bef,
          'cost_after'          => $cost_aft,
          'delivery_before'     => $delivery_bef,
          'delivery_after'      => $delivery_aft,
          'safety_before'       => $safety_bef,
          'safety_after'        => $safety_aft,
          'moral_before'        => $moral_bef,
          'moral_after'         => $moral_aft,
          'productivity_before' => $productivity_bef,
          'productivity_after'  => $productivity_aft,
          'environtment_before' => $env_bef,
          'environtment_after'  => $env_aft,
          'total_benefit' => $total_benefit,
          'id_laporan'    => $id_laporan,
          'nik' => $nik,
          'id_pertemuan' => $id_pertemuan
      
        );
        $this->UserModel->tambah_laporan_ss($data, 'laporan_perbaikan_ss');
  
       redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
      
       } else if($this->upload->do_upload('produk_bef') ==  false){
        $data = array(
          'produk_after'          => $pict2,
          'aktivitas_perbaikan' => $aktivitas_perbaikan,
          'biaya_imp'           => $biaya_imp,
          'perhitungan_benefit' => $perhitungan_benefit,
          'quality_before'      => $quality_bef,
          'quality_after'       => $quality_aft,
          'cost_before'         => $cost_bef,
          'cost_after'          => $cost_aft,
          'delivery_before'     => $delivery_bef,
          'delivery_after'      => $delivery_aft,
          'safety_before'       => $safety_bef,
          'safety_after'        => $safety_aft,
          'moral_before'        => $moral_bef,
          'moral_after'         => $moral_aft,
          'productivity_before' => $productivity_bef,
          'productivity_after'  => $productivity_aft,
          'environtment_before' => $env_bef,
          'environtment_after'  => $env_aft,
          'total_benefit' => $total_benefit,
          'id_laporan'    => $id_laporan,
          'nik' => $nik,
          'id_pertemuan' => $id_pertemuan
      
        );
        $this->UserModel->tambah_laporan_ss($data, 'laporan_perbaikan_ss');
  
       redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
      
       } else if($this->upload->do_upload('produk_bef') == true && $this->upload->do_upload('produk_aft') == true)
       {
        $data = array(
          'produk_before' => $pict,
          'produk_after'          => $pict2,
          'aktivitas_perbaikan' => $aktivitas_perbaikan,
          'biaya_imp'           => $biaya_imp,
          'perhitungan_benefit' => $perhitungan_benefit,
          'quality_before'      => $quality_bef,
          'quality_after'       => $quality_aft,
          'cost_before'         => $cost_bef,
          'cost_after'          => $cost_aft,
          'delivery_before'     => $delivery_bef,
          'delivery_after'      => $delivery_aft,
          'safety_before'       => $safety_bef,
          'safety_after'        => $safety_aft,
          'moral_before'        => $moral_bef,
          'moral_after'         => $moral_aft,
          'productivity_before' => $productivity_bef,
          'productivity_after'  => $productivity_aft,
          'environtment_before' => $env_bef,
          'environtment_after'  => $env_aft,
          'total_benefit' => $total_benefit,
          'id_laporan'    => $id_laporan,
          'nik' => $nik,
          'id_pertemuan' => $id_pertemuan
      
        );
        $this->UserModel->tambah_laporan_ss($data, 'laporan_perbaikan_ss');
  
       redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
       
      
      } else{
        $data = array(
          'aktivitas_perbaikan' => $aktivitas_perbaikan,
          'biaya_imp'           => $biaya_imp,
          'perhitungan_benefit' => $perhitungan_benefit,
          'quality_before'      => $quality_bef,
          'quality_after'       => $quality_aft,
          'cost_before'         => $cost_bef,
          'cost_after'          => $cost_aft,
          'delivery_before'     => $delivery_bef,
          'delivery_after'      => $delivery_aft,
          'safety_before'       => $safety_bef,
          'safety_after'        => $safety_aft,
          'moral_before'        => $moral_bef,
          'moral_after'         => $moral_aft,
          'productivity_before' => $productivity_bef,
          'productivity_after'  => $productivity_aft,
          'environtment_before' => $env_bef,
          'environtment_after'  => $env_aft,
          'total_benefit' => $total_benefit,
          'id_laporan'    => $id_laporan,
          'nik' => $nik,
          'id_pertemuan' => $id_pertemuan
      
        );
        $this->UserModel->tambah_laporan_ss($data, 'laporan_perbaikan_ss');
  
       redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
       
      }


    }
       
    }

    function update_laporan_ss($id_laporan)
    {

      $aktivitas_perbaikan = $this->input->post('aktivitas_perbaikan');
      $biaya_imp = $this->input->post('biaya_improvement');
      $perhitungan_benefit = $this->input->post('perhitungan_benefit');
      $quality_bef = $this->input->post('quality_before');
      $quality_aft = $this->input->post('quality_after');
      $cost_bef = $this->input->post('cost_before');
      $cost_aft = $this->input->post('cost_after');
      $delivery_bef = $this->input->post('delivery_before');
      $delivery_aft = $this->input->post('delivery_after');
      $safety_bef = $this->input->post('safety_before');
      $safety_aft = $this->input->post('safety_after');
      $moral_bef = $this->input->post('moral_before');
      $moral_aft = $this->input->post('moral_after');
      $productivity_bef = $this->input->post('productivity_before');
      $productivity_aft = $this->input->post('productivity_after');
      $env_bef = $this->input->post('environtment_before');
      $env_aft = $this->input->post('environtment_after');
      $id_laporan = $this->input->post('id_laporan');
      $id_pertemuan = $this->input->post('id_pertemuan');

        $config['max_size']      = 2048;
        $config['allowed_types'] = "png|jpg|jpeg|gif";
        $config['remove_spaces'] = TRUE;
        $config['overwrite']     = TRUE;
        $config['upload_path']   = FCPATH.'/upload';
        $config['file_name'] = $id_pertemuan.'_'.rand(1,100);


    $this->load->library('upload');
    $this->upload->initialize($config);
    $this->upload->do_upload('produk_bef');
    $data_image=$this->upload->data('file_name');
    $location=base_url().'upload/';
    $pict=$location.$data_image;

    $config2['max_size']      = 2048;
    $config2['allowed_types'] = "png|jpg|jpeg|gif";
    $config2['remove_spaces'] = TRUE;
    $config2['overwrite']     = TRUE;
    $config2['upload_path']   = FCPATH.'/upload';
    $config2['file_name'] = $id_pertemuan.'_'.rand(1,100);

    
    $this->load->library('upload');
    $this->upload->initialize($config2);
    $this->upload->do_upload('produk_aft');
    $data_image2=$this->upload->data('file_name');
    $location=base_url().'upload/';
    $pict2=$location.$data_image2;

    if($int_size1 > $config['max_size'] || $int_size2 > $config['max_size']){
      $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert"> Upload foto harus kurang dari 5mb <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      redirect('Atasan/dashboard_formlaporan/'.$id_pertemuan);
    
   } else{

    if($this->upload->do_upload('produk_aft') == false){
      $data = array(
        'produk_before'          =>$pict,
        'aktivitas_perbaikan' => $aktivitas_perbaikan,
        'biaya_imp'           => $biaya_imp,
        'perhitungan_benefit' => $perhitungan_benefit,
        'quality_before'      => $quality_bef,
        'quality_after'       => $quality_aft,
        'cost_before'         => $cost_bef,
        'cost_after'          => $cost_aft,
        'delivery_before'     => $delivery_bef,
        'delivery_after'      => $delivery_aft,
        'safety_before'       => $safety_bef,
        'safety_after'        => $safety_aft,
        'moral_before'        => $moral_bef,
        'moral_after'         => $moral_aft,
        'productivity_before' => $productivity_bef,
        'productivity_after'  => $productivity_aft,
        'environtment_before' => $env_bef,
        'environtment_after'  => $env_aft,
        'total_benefit' => $total_benefit,
        'id_laporan'    => $id_laporan,
        'nik' => $nik,
        'id_pertemuan' => $id_pertemuan
    
      );
      $this->UserModel->update_laporan_ss($data, 'laporan_perbaikan_ss' , $id_laporan);

      redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
    
     } else if($this->upload->do_upload('produk_bef') ==  false){
      $data = array(
        'produk_after'          => $pict2,
        'aktivitas_perbaikan' => $aktivitas_perbaikan,
        'biaya_imp'           => $biaya_imp,
        'perhitungan_benefit' => $perhitungan_benefit,
        'quality_before'      => $quality_bef,
        'quality_after'       => $quality_aft,
        'cost_before'         => $cost_bef,
        'cost_after'          => $cost_aft,
        'delivery_before'     => $delivery_bef,
        'delivery_after'      => $delivery_aft,
        'safety_before'       => $safety_bef,
        'safety_after'        => $safety_aft,
        'moral_before'        => $moral_bef,
        'moral_after'         => $moral_aft,
        'productivity_before' => $productivity_bef,
        'productivity_after'  => $productivity_aft,
        'environtment_before' => $env_bef,
        'environtment_after'  => $env_aft,
        'total_benefit' => $total_benefit,
        'id_laporan'    => $id_laporan,
        'nik' => $nik,
        'id_pertemuan' => $id_pertemuan
    
      );
      $this->UserModel->update_laporan_ss($data, 'laporan_perbaikan_ss' , $id_laporan);

      redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
    
     } else if($this->upload->do_upload('produk_bef') == true && $this->upload->do_upload('produk_aft') == true)
     {
      $data = array(
        'produk_before' => $pict,
        'produk_after'          => $pict2,
        'aktivitas_perbaikan' => $aktivitas_perbaikan,
        'biaya_imp'           => $biaya_imp,
        'perhitungan_benefit' => $perhitungan_benefit,
        'quality_before'      => $quality_bef,
        'quality_after'       => $quality_aft,
        'cost_before'         => $cost_bef,
        'cost_after'          => $cost_aft,
        'delivery_before'     => $delivery_bef,
        'delivery_after'      => $delivery_aft,
        'safety_before'       => $safety_bef,
        'safety_after'        => $safety_aft,
        'moral_before'        => $moral_bef,
        'moral_after'         => $moral_aft,
        'productivity_before' => $productivity_bef,
        'productivity_after'  => $productivity_aft,
        'environtment_before' => $env_bef,
        'environtment_after'  => $env_aft,
        'total_benefit' => $total_benefit,
        'id_laporan'    => $id_laporan,
        'nik' => $nik,
        'id_pertemuan' => $id_pertemuan
    
      );
      $this->UserModel->update_laporan_ss($data, 'laporan_perbaikan_ss' , $id_laporan);

      redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
     
    
    } else{
      $data = array(
        'aktivitas_perbaikan' => $aktivitas_perbaikan,
        'biaya_imp'           => $biaya_imp,
        'perhitungan_benefit' => $perhitungan_benefit,
        'quality_before'      => $quality_bef,
        'quality_after'       => $quality_aft,
        'cost_before'         => $cost_bef,
        'cost_after'          => $cost_aft,
        'delivery_before'     => $delivery_bef,
        'delivery_after'      => $delivery_aft,
        'safety_before'       => $safety_bef,
        'safety_after'        => $safety_aft,
        'moral_before'        => $moral_bef,
        'moral_after'         => $moral_aft,
        'productivity_before' => $productivity_bef,
        'productivity_after'  => $productivity_aft,
        'environtment_before' => $env_bef,
        'environtment_after'  => $env_aft,
        'total_benefit' => $total_benefit,
        'id_laporan'    => $id_laporan,
        'nik' => $nik,
        'id_pertemuan' => $id_pertemuan
    
      );
      $this->UserModel->update_laporan_ss($data, 'laporan_perbaikan_ss' , $id_laporan);

      redirect('Atasan/DetailPertemuanSS/'.$id_pertemuan);
     
    }
  }
    
   

  
    }


  




    
 

}

/* End of file Atasan.php */
