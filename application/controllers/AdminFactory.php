<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminFactory extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->model('AdminFactoryModel');
        $this->load->model('AtasanModel');
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->db1 = $CI->load->database('default',TRUE);
        
        date_default_timezone_set('Asia/Jakarta');
		
    }
    

    public function index()
    {
        $this->load->view('ss/admin_factory/daftar_ss');
        
    }
	
    function list_pertemuan()
    {
        $columns = array(
					0 => '',
					1 =>'name',
					2 =>'judul',
					3 =>'date_created',
					4 =>'meeting_date',
					5 =>'department_name',
					6 =>'factory',
					7 =>'approve_spv',
					8 =>'approve_subdept_head',
					9 =>'approve_dept_head',
					10 =>'approve_komite' 
            
        );
     		// var_dump($this->input->post('start'));
				//  die();
     
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->AdminFactoryModel->allposts_count_pertemuan();


        $totalFiltered = $totalData; 

        if(empty($this->input->post('search')['value']))
        {            
        $master = $this->AdminFactoryModel->allposts_pertemuan($limit,$start,$order,$dir);
        }
        else {
					$search = $this->input->post('search')['value'];
					
					// explode buat misah 
					$dataFilter = explode('^', $search);

					// Explode buat misah bulan awal dan bulan akhir nanti dimodel tinggal dikasih range between
					
					// var_dump($dataFilter);
					// die();
					
					$master =  $this->AdminFactoryModel->posts_search_pertemuan($limit,$start,$search,$order,$dir, $dataFilter);
					
					// var_dump($master);
					// die();

					$totalFiltered = $this->AdminFactoryModel->posts_search_count_pertemuan($search, $dataFilter);
					}	

					$data = array();
					$nomor_urut = 0;
					if(!empty($master))
					{
					foreach ($master as $key=>$master)
					{
					$_temp = '"'.$master->id_pertemuan.'"';
					if($master->nik_spv != null){
														
							if($master->approve_spv == 0){
								$spv = "Belum di approve";
							} else if($master->approve_spv == 1){
								if($master->tgl_app_spv != null && $master->tgl_app_spv == "1"){
									$spv = "Approved at ". $master->tgl_app_spv;
								} else{
									$spv = "Approved ";
								}
								
							} else{
									$spv = "Ditolak";
							}    
						}else{
							$spv = "-";
						}
					if($master->nik_subdept_head != null){
																
							if($master->approve_subdept_head == 0){
								$sub = "Belum di approve";
							} else if($master->approve_subdept_head == 1){
								if($master->tgl_app_sub != null && $master->tgl_app_sub == "1"){
									$sub = "Approved at ". $master->tgl_app_sub;
								} else{
									$sub = "Approved ";
								}
								
							} else{
									$sub = "Ditolak";
							} 
						} else{
							$sub = "-";
						} 

					if($master->nik_dept_head != null){
																
							if($master->approve_dept_head == 0){
								$dept = "Belum di approve";
							} else if($master->approve_dept_head == 1){
								if($master->tgl_app_dept != null && $master->tgl_app_dept == "1"){
									$dept = "Approved at ". $master->tgl_app_dept;
								} else{
									$dept = "Approved ";
								}
								
							} else{
									$dept = "Ditolak";
							}
						}else{
							$dept = "-";
						}
					if($master->approve_komite == 0){
							$komite =  "Belum di approve";
					} else if($master->approve_komite == 1){
						if($master->tgl_app_kom != null && $master->tgl_app_kom == "1"){
							$komite = "Approved at ". $master->tgl_app_kom;
						} else{
							$komite = "Approved ";
						}
						
					} else{
							$komite = "Ditolak";
					} 

					$tgl_dibuat = date("d-m-Y", strtotime($master->date_created));
					
					$tgl_perbaikan = date("d-m-Y", strtotime($master->meeting_date));

					$nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
					$nestedData['name']                 = ucwords($master->name);
					$nestedData['judul']                = ucwords($master->judul);
					$nestedData['date_created']         = ucwords($tgl_dibuat);
					$nestedData['meeting_date']         = ucwords($tgl_perbaikan);
					$nestedData['department_name']      = ucwords($master->department_name);
					$nestedData['factory']              = ucwords($master->factory);
					$nestedData['approve_spv']          = ucwords($spv);
					$nestedData['approve_subdept_head'] = ucwords($sub);
					$nestedData['approve_dept_head']    = ucwords($dept);
					$nestedData['approve_komite']       = ucwords($komite);
					$nestedData['action']               = "<center><a href='".base_url()."AdminFactory/DetailPertemuanSS/$master->id_pertemuan' class='btn btn-xs btn-info'>  <i class='fas fa-edit'></i></a>
					</center>";
					// $nestedData['action']               = "<center><a href='".base_url()."Komite/DetailPertemuanSS/$master->id_pertemuan' class='btn btn-xs btn-info'>  <i class='fas fa-edit'></i></a> 
					// <button type='button' class='btn btn-success' data-toggle='modal' data-target='#modal-default'><i class='fas fa-trash'></i></center>";
							
					$data[] = $nestedData;

        }
        }
      

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

				// var_dump($data);
				// die();
    
        echo json_encode($json_data);
    }


    public function DetailPertemuanSS($id_pertemuan)
    {
        $data['detail']        = $this->AdminModel->DetailPertemuanSS($id_pertemuan);
        $data['nilai_spv']     = $this->AdminModel->DetailNilaiSS_spv($id_pertemuan);
        $data['nilai_subdept'] = $this->AdminModel->DetailNilaiSS_subdept_head($id_pertemuan);
        $data['nilai_dept']    = $this->AdminModel->DetailNilaiSS_dept_head($id_pertemuan);
        $data['nilai_komite']  = $this->AdminModel->DetailNilaiSS_komite($id_pertemuan);
		// var_dump($data['detail']);
		// die();
        $this->load->view('ss/admin_factory/detail_ss', $data);
        
    }
}

/* End of file Controllername.php */
