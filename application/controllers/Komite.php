<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Komite extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->model('AtasanModel');
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->db1 = $CI->load->database('default',TRUE);
        
        date_default_timezone_set('Asia/Jakarta');
		
    }
    

    public function index()
    {
        $this->load->view('ss/komite/daftar_ss');
        
    }
	
    function list_pertemuan()
    {
        $columns = array(
					0 => '',
					1 =>'name',
					2 =>'judul',
					3 =>'date_created',
					4 =>'meeting_date',
					5 =>'department_name',
					6 =>'factory',
					7 =>'approve_spv',
					8 =>'approve_subdept_head',
					9 =>'approve_dept_head',
					10 =>'approve_komite' 
            
        );
     		// var_dump($this->input->post('start'));
				//  die();
     
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];
        $draw  = $this->input->post('draw');

        $totalData = $this->AdminModel->allposts_count_pertemuan();


        $totalFiltered = $totalData; 

        if(empty($this->input->post('search')['value']))
        {            
        $master = $this->AdminModel->allposts_pertemuan($limit,$start,$order,$dir);
        }
        else {
					$search = $this->input->post('search')['value'];
					
					// explode buat misah 
					$dataFilter = explode('^', $search);

					// Explode buat misah bulan awal dan bulan akhir nanti dimodel tinggal dikasih range between
					
					// var_dump($dataFilter);
					// die();
					
					$master =  $this->AdminModel->posts_search_pertemuan($limit,$start,$search,$order,$dir, $dataFilter);
					
					// var_dump($master);
					// die();

					$totalFiltered = $this->AdminModel->posts_search_count_pertemuan($search, $dataFilter);
					}	

					$data = array();
					$nomor_urut = 0;
					if(!empty($master))
					{
					foreach ($master as $key=>$master)
					{
					$_temp = '"'.$master->id_pertemuan.'"';
					if($master->nik_spv != null){
														
							if($master->approve_spv == 0){
								$spv = "Belum di approve";
							} else if($master->approve_spv == 1){
								if($master->tgl_app_spv != null && $master->tgl_app_spv == "1"){
									$spv = "Approved at ". $master->tgl_app_spv;
								} else{
									$spv = "Approved ";
								}
								
							} else{
									$spv = "Ditolak";
							}    
						}else{
							$spv = "-";
						}
					if($master->nik_subdept_head != null){
																
							if($master->approve_subdept_head == 0){
								$sub = "Belum di approve";
							} else if($master->approve_subdept_head == 1){
								if($master->tgl_app_sub != null && $master->tgl_app_sub == "1"){
									$sub = "Approved at ". $master->tgl_app_sub;
								} else{
									$sub = "Approved ";
								}
								
							} else{
									$sub = "Ditolak";
							} 
						} else{
							$sub = "-";
						} 

					if($master->nik_dept_head != null){
																
							if($master->approve_dept_head == 0){
								$dept = "Belum di approve";
							} else if($master->approve_dept_head == 1){
								if($master->tgl_app_dept != null && $master->tgl_app_dept == "1"){
									$dept = "Approved at ". $master->tgl_app_dept;
								} else{
									$dept = "Approved ";
								}
								
							} else{
									$dept = "Ditolak";
							}
						}else{
							$dept = "-";
						}
					if($master->approve_komite == 0){
							$komite =  "Belum di approve";
					} else if($master->approve_komite == 1){
						if($master->tgl_app_kom != null && $master->tgl_app_kom == "1"){
							$komite = "Approved at ". $master->tgl_app_kom;
						} else{
							$komite = "Approved ";
						}
						
					} else{
							$komite = "Ditolak";
					} 

					$tgl_dibuat = date("d-m-Y", strtotime($master->date_created));
					
					$tgl_perbaikan = date("d-m-Y", strtotime($master->meeting_date));

					$nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
					$nestedData['name']                 = ucwords($master->name);
					$nestedData['judul']                = ucwords($master->judul);
					$nestedData['date_created']         = ucwords($tgl_dibuat);
					$nestedData['meeting_date']         = ucwords($tgl_perbaikan);
					$nestedData['department_name']      = ucwords($master->department_name);
					$nestedData['factory']              = ucwords($master->factory);
					$nestedData['approve_spv']          = ucwords($spv);
					$nestedData['approve_subdept_head'] = ucwords($sub);
					$nestedData['approve_dept_head']    = ucwords($dept);
					$nestedData['approve_komite']       = ucwords($komite);
					$nestedData['action']               = "<center><a href='".base_url()."Komite/DetailPertemuanSS/$master->id_pertemuan' class='btn btn-xs btn-info'>  <i class='fas fa-edit'></i></a>
					<a href='#' data-id='$master->id_pertemuan' class='btn btn-xs btn-danger' id='buttonDeletePertemuan'>  <i class='fas fa-trash'></i></a> </center>";
					// $nestedData['action']               = "<center><a href='".base_url()."Komite/DetailPertemuanSS/$master->id_pertemuan' class='btn btn-xs btn-info'>  <i class='fas fa-edit'></i></a> 
					// <button type='button' class='btn btn-success' data-toggle='modal' data-target='#modal-default'><i class='fas fa-trash'></i></center>";
							
					$data[] = $nestedData;

        }
        }
      

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

				// var_dump($data);
				// die();
    
        echo json_encode($json_data);
    }


    public function DetailPertemuanSS($id_pertemuan)
    {
        $data['detail']        = $this->AdminModel->DetailPertemuanSS($id_pertemuan);
        $data['nilai_spv']     = $this->AdminModel->DetailNilaiSS_spv($id_pertemuan);
        $data['nilai_subdept'] = $this->AdminModel->DetailNilaiSS_subdept_head($id_pertemuan);
        $data['nilai_dept']    = $this->AdminModel->DetailNilaiSS_dept_head($id_pertemuan);
        $data['nilai_komite']  = $this->AdminModel->DetailNilaiSS_komite($id_pertemuan);
		// var_dump($data['detail']);
		// die();
        $this->load->view('ss/komite/detail_ss', $data);
        
    }

    public function approve_permintaan_ss($id_pertemuan)
    {
        $this->AdminModel->approve_permintaan_ss($id_pertemuan); 
          $data = array(
            // 'id_approval' => rand(1,300),
            'id_pertemuan' => $id_pertemuan,
            'create_by' => $this->session->userdata('nik'),
            'create_date' => date('Y-m-d H:i:s'),
            'approval' => '1'
        );
        $this->db2->insert('approval', $data); 
		$this->cek_permintaan_ss($id_pertemuan);

        
        redirect('Komite/DetailPertemuanSS/'.$id_pertemuan);
        
    }

	public function reject_permintaan_ss($id_pertemuan)
    {
         $this->AdminModel->reject_permintaan_ss($id_pertemuan);  
         $data = array(
          // 'id_approval' => rand(1,300),
          'id_pertemuan' => $id_pertemuan,
          'create_by' => $this->session->userdata('nik'),
          'create_date' => date('Y-m-d H:i:s'),
          'approval' => '2'
      );
      $this->db2->insert('approval', $data);
	  $this->cek_permintaan_ss($id_pertemuan);
 
      
      redirect('Komite/DetailPertemuanSS/'.$id_pertemuan);
    }

	// Validasi Approval Pertemuan
	public function cek_permintaan_ss($id_pertemuan){
		$userNik = $this->session->userdata('nik');

		$sql = "SELECT * FROM approval  WHERE id_pertemuan = '$id_pertemuan' AND create_by = '$userNik'";
		$query = $this->db2->query($sql);

		// var_dump($query->result()[0]);
		// die();

		if($query->num_rows() > 1){
			for($i=0;$i<$query->num_rows();$i++){
				if($i>0){
					$dataId = $query->result()[$i];
					$sql = "DELETE FROM approval  WHERE id = '$dataId->id'";
					$delQuery = $this->db2->query($sql);
				}
			}
		}
	}


    public function dashboard_formpenilaian($id_pertemuan)
    {
      
        $nik = $this->session->userdata('nik');
        $sql = "SELECT * FROM nilai_improvement_ss WHERE id_pertemuan = '$id_pertemuan' AND nik = '$nik'";
        $query = $this->db2->query($sql)->result();
       
        if($query != NULL){
            $data['cek'] = 1;
            $data['nilai'] = $query;
        } else{
            $data['cek'] = 2;
            $data['id_pertemuan'] = $id_pertemuan;
        }
        
        $this->load->view('ss/komite/add_nilai', $data);   
    }

    public function simpan_nilai_ss($id_pertemuan)
    {
        $nik                 = $this->session->userdata('nik');
        $penghematan_biaya   = $this->input->post('penghematan_biaya');
        $penghematan_manhour = $this->input->post('penghematan_manhour');
        $safety              = $this->input->post('safety');
        $ergonomi            = $this->input->post('ergonomi');
        $quality             = $this->input->post('quality');
        $manfaat             = $this->input->post('manfaat');
        $keaslian            = $this->input->post('keaslian');
        $usaha               = $this->input->post('usaha');
        $komentar            = $this->input->post('komentar');
        $reward            = $this->input->post('optionsRadios');
       
        $date = date('Y-m-d');

        // buat id nilai
        $id_nilai = 'NI'.$id_pertemuan.'_'.$nik;


       
        $sql = "SELECT id_nilai FROM nilai_improvement_ss WHERE nik = '$nik' AND id_pertemuan = '$id_pertemuan'";
        $query = $this->db2->query($sql)->row();

        if($query != NULL){
            // total nilai
    //   if(is_numeric($penghematan_biaya) && is_numeric($penghematan_manhour) && is_numeric($safety)
    //   && is_numeric($ergonomi) && is_numeric($quality) && is_numeric($manfaat) && is_numeric($keaslian)
    //   && is_numeric($usaha)) : 
      $total_nilai = $penghematan_biaya + $penghematan_manhour + $safety  + $ergonomi + $quality
              + $manfaat + $keaslian  + $usaha;
       
              $get_id_nilai = $this->input->post('id_nilai');
              $data = array(
                  'id_nilai'            => $get_id_nilai,
                  'penghematan_biaya'   => $penghematan_biaya,
                  'penghematan_manhour' => $penghematan_manhour,
                  'safety'              => $safety,
                  'ergonomi'            => $ergonomi,
                  'quality'             => $quality,
                  'manfaat'             => $manfaat,
                  'keaslian'            => $keaslian,
                  'usaha'               => $usaha,
                  'total_nilai'         => $total_nilai,
                  'tanggal_penilaian'   => $date,
                  'reward' => $reward
              );
              $this->AtasanModel->update_nilai_ss($data, 'nilai_improvement_ss', $id_nilai);
              
              redirect('Komite/DetailPertemuanSS/'.$id_pertemuan);   
    //   else :
    //       echo "-";
    //   endif;
        

      } else{
            // total nilai
    //   if(is_numeric($penghematan_biaya) && is_numeric($penghematan_manhour) && is_numeric($safety)
    //   && is_numeric($ergonomi) && is_numeric($quality) && is_numeric($manfaat) && is_numeric($keaslian)
    //   && is_numeric($usaha)) : 
      $total_nilai = $penghematan_biaya + $penghematan_manhour + $safety  + $ergonomi + $quality
              + $manfaat + $keaslian  + $usaha;
                $data = array(
                  'id_nilai'            => $id_nilai,
                  'id_pertemuan'        => $id_pertemuan,
                  'nik'                 => $nik,
                  'penghematan_biaya'   => $penghematan_biaya,
                  'penghematan_manhour' => $penghematan_manhour,
                  'safety'              => $safety,
                  'ergonomi'            => $ergonomi,
                  'quality'             => $quality,
                  'manfaat'             => $manfaat,
                  'keaslian'            => $keaslian,
                  'usaha'               => $usaha,
                  'total_nilai'         => $total_nilai,
                  'tanggal_penilaian' => $date,
                  'reward'   => $reward
              );
            //   var_dump($data);
            //   die();
           
              $this->AtasanModel->simpan_nilai_ss($data, 'nilai_improvement_ss');
              
              redirect('Komite/DetailPertemuanSS/'.$id_pertemuan);
    
        
          
      }
    }

    public function print($id_pertemuan)
    {
     
        $data['pertemuan'] = $this->AtasanModel->data_pertemuan($id_pertemuan);
        $data['laporan'] = $this->AtasanModel->data_laporan_perbaikan($id_pertemuan);
        $data['nilai_spv'] = $this->AtasanModel->data_nilai_ss_spv($id_pertemuan);
        $data['nilai_subdept'] = $this->AtasanModel->data_nilai_ss_subdept($id_pertemuan);
        $data['nilai_depthead'] = $this->AtasanModel->data_nilai_ss_depthead($id_pertemuan);
        $data['nilai_komite'] = $this->AtasanModel->data_nilai_ss_komite($id_pertemuan);
        $data['nama_spv'] = $this->AtasanModel->getnama_spv($id_pertemuan);
        $data['nama_subdept'] = $this->AtasanModel->getnama_subdept($id_pertemuan);
        $data['nama_dept'] = $this->AtasanModel->getnama_dept($id_pertemuan);
        $data['nama_komite'] = $this->AtasanModel->getnama_komite($id_pertemuan);
		$this->db2->where('id_pertemuan', $id_pertemuan);
		$this->db2->from('pertemuan_improvement_ss');
		$data['meeting_date'] = $this->db2->get()->row_array();
	

        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'vertikal');
        $this->pdf->filename = "Form.pdf";
        $this->pdf->load_view('ss/HasilPenilaian', $data);   
  
    }

    public function export()
    {
    
  		//  include $_SERVER["DOCUMENT_ROOT"].'/lumen/application/third_party/PHPExcel/PHPExcel.php';
			include $_SERVER["DOCUMENT_ROOT"].'/application/third_party/PHPExcel/PHPExcel.php';
			
				// Panggil class PHPExcel nya
				$excel = new PHPExcel();
				// Settingan awal fil excel
				$excel->getProperties()->setCreator('ICT Team')
										->setLastModifiedBy('ICT Team')
										->setTitle("Data Report")
										->setSubject("Laporan SS")
										->setDescription("Laporan SS")
										->setKeywords("Data SS");
				// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
				$style_col = array(
					'font' => array('bold' => true), // Set font nya jadi bold
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
					),
					'borders' => array(
						'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
						'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
						'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
						'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
					)
				);
				// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
				$style_row = array(
					'alignment' => array(
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
					),
					'borders' => array(
						'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
						'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
						'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
						'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
					)
				);
				$excel->setActiveSheetIndex(0)->setCellValue('A1', "Laporan Data SS"); // Set kolom A1 dengan tulisan "DATA SISWA"
				$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
				$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
				$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
				$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
				// Buat header tabel nya pada baris ke 3
					$excel->setActiveSheetIndex(0)->setCellValue('A4', "NO REGISTER SS"); // Set kolom A3 dengan tulisan "NO"
					$excel->setActiveSheetIndex(0)->setCellValue('B4', "NIK"); // Set kolom B3 dengan tulisan "NIS"
					$excel->setActiveSheetIndex(0)->setCellValue('C4', "NAMA"); // Set kolom C3 dengan tulisan "NAMA"
					$excel->setActiveSheetIndex(0)->setCellValue('D4', "DEPARTMENT"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
					$excel->setActiveSheetIndex(0)->setCellValue('E4', "SUB DEPARTMENT"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
					$excel->setActiveSheetIndex(0)->setCellValue('F4', "FACTORY");
					$excel->setActiveSheetIndex(0)->setCellValue('G4', "STATUS KARYAWAN");
					$excel->setActiveSheetIndex(0)->setCellValue('H4', "JUDUL"); // Set kolom E3 dengan tulisan "ALAMAT"
					$excel->setActiveSheetIndex(0)->setCellValue('I4', "TANGGAL SS");
					$excel->setActiveSheetIndex(0)->setCellValue('J4', "TOTAL BENEFIT");
					$excel->setActiveSheetIndex(0)->setCellValue('K4', "REWARD");
					$excel->setActiveSheetIndex(0)->setCellValue('L4', "HADIAH");
					$excel->setActiveSheetIndex(0)->setCellValue('M4', "APP SPV");
					$excel->setActiveSheetIndex(0)->setCellValue('N4', "APP SUB DEPT");
					$excel->setActiveSheetIndex(0)->setCellValue('O4', "APP DEPT");
					$excel->setActiveSheetIndex(0)->setCellValue('P4', "APP KOMITE");
					$excel->setActiveSheetIndex(0)->setCellValue('Q4', "TGL NILAI SPV");
					$excel->setActiveSheetIndex(0)->setCellValue('R3', "NILAI SPV");
					$excel->setActiveSheetIndex(0)->setCellValue('R4', "PENGHEMATAN BIAYA");
					$excel->setActiveSheetIndex(0)->setCellValue('S4', "PENGHEMATAN MAN HOUR");
					$excel->setActiveSheetIndex(0)->setCellValue('T4', "SAFETY"); 
					$excel->setActiveSheetIndex(0)->setCellValue('U4', "ERGONOMI");
					$excel->setActiveSheetIndex(0)->setCellValue('V4', "QUALITY");
					$excel->setActiveSheetIndex(0)->setCellValue('W4', "MANFAAT");
					$excel->setActiveSheetIndex(0)->setCellValue('X4', "KEASLIAN");
					$excel->setActiveSheetIndex(0)->setCellValue('Y4', "USAHA");
					$excel->setActiveSheetIndex(0)->setCellValue('Z4', "TGL NILAI SUBDEPT");
					$excel->setActiveSheetIndex(0)->setCellValue('AA3', "NILAI SUB DEPT");
					$excel->setActiveSheetIndex(0)->setCellValue('AA4', "PENGHEMATAN BIAYA");
					$excel->setActiveSheetIndex(0)->setCellValue('AB4', "PENGHEMATAN MAN HOUR");
					$excel->setActiveSheetIndex(0)->setCellValue('AC4', "SAFETY"); 
					$excel->setActiveSheetIndex(0)->setCellValue('AD4', "ERGONOMI");
					$excel->setActiveSheetIndex(0)->setCellValue('AE4', "QUALITY");
					$excel->setActiveSheetIndex(0)->setCellValue('AF4', "MANFAAT");
					$excel->setActiveSheetIndex(0)->setCellValue('AG4', "KEASLIAN");
					$excel->setActiveSheetIndex(0)->setCellValue('AH4', "USAHA");
					$excel->setActiveSheetIndex(0)->setCellValue('AI4', "TGL NILAI DEPTHEAD");
					$excel->setActiveSheetIndex(0)->setCellValue('AJ3', "NILAI DEPT HEAD");
					$excel->setActiveSheetIndex(0)->setCellValue('AJ4', "PENGHEMATAN BIAYA");
					$excel->setActiveSheetIndex(0)->setCellValue('AK4', "PENGHEMATAN MAN HOUR");
					$excel->setActiveSheetIndex(0)->setCellValue('AL4', "SAFETY"); 
					$excel->setActiveSheetIndex(0)->setCellValue('AM4', "ERGONOMI");
					$excel->setActiveSheetIndex(0)->setCellValue('AN4', "QUALITY");
					$excel->setActiveSheetIndex(0)->setCellValue('AO4', "MANFAAT");
					$excel->setActiveSheetIndex(0)->setCellValue('AP4', "KEASLIAN");
					$excel->setActiveSheetIndex(0)->setCellValue('AQ4', "USAHA");
					$excel->setActiveSheetIndex(0)->setCellValue('AR4', "TGL NILAI KOMITE");
					$excel->setActiveSheetIndex(0)->setCellValue('AS3', "NILAI KOMITE");
					$excel->setActiveSheetIndex(0)->setCellValue('AS4', "PENGHEMATAN BIAYA");
					$excel->setActiveSheetIndex(0)->setCellValue('AT4', "PENGHEMATAN MAN HOUR");
					$excel->setActiveSheetIndex(0)->setCellValue('AU4', "SAFETY"); 
					$excel->setActiveSheetIndex(0)->setCellValue('AV4', "ERGONOMI");
					$excel->setActiveSheetIndex(0)->setCellValue('AW4', "QUALITY");
					$excel->setActiveSheetIndex(0)->setCellValue('AX4', "MANFAAT");
					$excel->setActiveSheetIndex(0)->setCellValue('AY4', "KEASLIAN");
					$excel->setActiveSheetIndex(0)->setCellValue('AZ4', "USAHA");
				// Apply style header yang telah kita buat tadi ke masing-masing kolom header
					$excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('C4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('F4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('G4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('H4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('I4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('J4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('K4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('L4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('M4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('N4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('O4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('P4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('Q4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('R4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('S4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('T4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('U4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('V4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('W4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('X4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('Y4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('Z4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AA4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AB4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AC4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AD4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AE4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AF4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AG4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AH4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AI4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AJ4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AK4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AL4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AM4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AN4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AO4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AP4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AQ4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AR4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AS4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AT4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AU4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AV4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AW4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AX4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AY4')->applyFromArray($style_col);
					$excel->getActiveSheet()->getStyle('AZ4')->applyFromArray($style_col);
			
			
				$department = $this->input->post('department');
				// var_dump($department);
				// die();
				$factory = $this->input->post('factory');
				$tgl_awal = $this->input->post('tgl_awal');
				$tgl_akhir = $this->input->post('tgl_akhir');
				$factory_id = 0;
				if($factory == "AOI1")
				{
					$factory_id = 1;
				}else if($factory == "AOI2")
				{
					$factory_id = 2;
				}else if($factory == "BBIS" || $factory == "AOI3")
				{
					$factory_id = 3;
				}else{
					$factory_id = 4;
				}


				$export = $this->AdminModel->export_laporan($department,$factory_id,$tgl_awal,$tgl_akhir);
				$no = 1; // Untuk penomoran tabel, di awal set dengan 1
				$numrow = 5; // Set baris pertama untuk isi tabel adalah baris ke 4
				//   var_dump($export);
				//   die();
				//  if($data->approve_spv == '0'){
				//   echo "-";
				// } else if($data->approve_spv == '1'){
				//   echo "Approved pada ".$data->tgl_app_spv;
				// } else{
				//   echo "Ditolak pada ".$data->tgl_app_spv;
				// });
				foreach($export as $data){ // Lakukan looping pada variabel siswa
					if($data->approve_spv == '0'){
						$ket_spv = "-";
					} else if($data->approve_spv == '1'){
						$ket_spv = "Approve ". date('d-m-Y', strtotime($data->tgl_app_spv)) ;
					} else{
						$ket_spv = "Ditolak ". date('d-m-Y', strtotime($data->tgl_app_spv));
					}

					if($data->approve_subdept_head == '0'){
						$ket_sub = "-";
					} else if($data->approve_subdept_head == '1'){
						$ket_sub = "Approve ". date('d-m-Y', strtotime($data->tgl_app_sub));
					} else{
						$ket_sub = "Ditolak ". date('d-m-Y', strtotime($data->tgl_app_sub));
					}
					if($data->approve_dept_head == '0'){
						$ket_dept = "-";
					} else if($data->approve_dept_head == '1'){
						$ket_dept = "Approve ". date('d-m-Y', strtotime($data->tgl_app_dept));
					} else{
						$ket_dept = "Ditolak ". date('d-m-Y', strtotime($data->tgl_app_dept));
					}
					if($data->approve_komite == '0'){
						$ket_komite = "-";
					} else if($data->approve_komite == '1'){
						$ket_komite = "Approve ". date('d-m-Y', strtotime($data->tgl_app_komite));
					} else{
						$ket_komite = "Ditolak ". date('d-m-Y', strtotime($data->tgl_app_komite));
					}
					if($data->tgl_nilai_spv != null){
							$tgl_spv = "Diisi pada ".date('d-m-Y', strtotime($data->tgl_nilai_spv));
					} else{
						$tgl_spv = "Belum Mengisi";
					}
					if($data->tgl_nilai_sub != null){
						$tgl_sub = "Diisi pada ".date('d-m-Y', strtotime($data->tgl_nilai_sub));
					} else{
						$tgl_sub = "Belum Mengisi";
					}
					if($data->tgl_nilai_dept != null){
					$tgl_dept = "Diisi pada ".date('d-m-Y', strtotime($data->tgl_nilai_dept));
					} else{
						$tgl_dept = "Belum Mengisi";
					}
					if($data->tgl_nilai_komite != null){
						$tgl_kom = "Diisi pada ".date('d-m-Y', strtotime($data->tgl_nilai_komite));
						} else{
							$tgl_kom = "Belum Mengisi";
					}

					// Set Status Karyawan
					// $status_karyawan = 'Active';
					// if($data->status_karyawan == '0'){
					// 	$status_karyawan = 'Inactive';
					// }

					$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $data->no_register);
					$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nik);
					$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->name);
					$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->department_name);
					$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->subdepartment_name);
					$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->factory);
					$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, '-');
					$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->judul);
					$excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, date('d-m-Y', strtotime($data->meeting_date)));
					$excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->total_benefit);
					$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->reward);
					$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $data->hadiah);
					$excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow,  $ket_spv);
					$excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow,  $ket_sub);
					$excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow,  $ket_dept);
					$excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow,  $ket_komite);
					$excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow,  $tgl_spv);
					$excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $data->spv_hematbiaya);
					$excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $data->spv_manhour);
					$excel->setActiveSheetIndex(0)->setCellValue('T'.$numrow, $data->spv_safety);
					$excel->setActiveSheetIndex(0)->setCellValue('U'.$numrow, $data->spv_ergonomi);
					$excel->setActiveSheetIndex(0)->setCellValue('V'.$numrow, $data->spv_quality);
					$excel->setActiveSheetIndex(0)->setCellValue('W'.$numrow, $data->spv_manfaat);
					$excel->setActiveSheetIndex(0)->setCellValue('X'.$numrow, $data->spv_keaslian);
					$excel->setActiveSheetIndex(0)->setCellValue('Y'.$numrow, $data->spv_usaha);
					$excel->setActiveSheetIndex(0)->setCellValue('Z'.$numrow,  $tgl_sub);
					$excel->setActiveSheetIndex(0)->setCellValue('AA'.$numrow, $data->subdept_hematbiaya);
					$excel->setActiveSheetIndex(0)->setCellValue('AB'.$numrow, $data->subdept_manhour);
					$excel->setActiveSheetIndex(0)->setCellValue('AC'.$numrow, $data->subdept_safety);
					$excel->setActiveSheetIndex(0)->setCellValue('AD'.$numrow, $data->subdept_ergonomi);
					$excel->setActiveSheetIndex(0)->setCellValue('AE'.$numrow, $data->subdept_quality);
					$excel->setActiveSheetIndex(0)->setCellValue('AF'.$numrow, $data->subdept_manfaat);
					$excel->setActiveSheetIndex(0)->setCellValue('AG'.$numrow, $data->subdept_keaslian);
					$excel->setActiveSheetIndex(0)->setCellValue('AH'.$numrow, $data->subdept_usaha); 
					$excel->setActiveSheetIndex(0)->setCellValue('AI'.$numrow,   $tgl_dept);   
					$excel->setActiveSheetIndex(0)->setCellValue('AJ'.$numrow, $data->dept_hematbiaya);
					$excel->setActiveSheetIndex(0)->setCellValue('AK'.$numrow, $data->dept_manhour);
					$excel->setActiveSheetIndex(0)->setCellValue('AL'.$numrow, $data->dept_safety);
					$excel->setActiveSheetIndex(0)->setCellValue('AM'.$numrow, $data->dept_ergonomi);
					$excel->setActiveSheetIndex(0)->setCellValue('AN'.$numrow, $data->dept_quality);
					$excel->setActiveSheetIndex(0)->setCellValue('AO'.$numrow, $data->dept_manfaat);
					$excel->setActiveSheetIndex(0)->setCellValue('AP'.$numrow, $data->dept_keaslian);
					$excel->setActiveSheetIndex(0)->setCellValue('AQ'.$numrow, $data->dept_usaha);
							
					$excel->setActiveSheetIndex(0)->setCellValue('AR'.$numrow,   $tgl_kom);
					$excel->setActiveSheetIndex(0)->setCellValue('AS'.$numrow, $data->komite_hematbiaya);
					$excel->setActiveSheetIndex(0)->setCellValue('AT'.$numrow, $data->komite_manhour);
					$excel->setActiveSheetIndex(0)->setCellValue('AU'.$numrow, $data->komite_safety);
					$excel->setActiveSheetIndex(0)->setCellValue('AV'.$numrow, $data->komite_ergonomi);
					$excel->setActiveSheetIndex(0)->setCellValue('AW'.$numrow, $data->komite_quality);
					$excel->setActiveSheetIndex(0)->setCellValue('AX'.$numrow, $data->komite_manfaat);
					$excel->setActiveSheetIndex(0)->setCellValue('AY'.$numrow, $data->komite_keaslian);
					$excel->setActiveSheetIndex(0)->setCellValue('AZ'.$numrow, $data->komite_usaha);
					// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
					$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('T'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('U'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('V'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('W'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('X'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('Y'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('Z'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AA'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AB'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AC'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AD'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AE'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AF'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AG'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AH'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AI'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AJ'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AK'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AL'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AM'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AN'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AO'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AP'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AQ'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AR'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AS'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AT'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AU'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AV'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AW'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AX'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AY'.$numrow)->applyFromArray($style_row);
					$excel->getActiveSheet()->getStyle('AZ'.$numrow)->applyFromArray($style_row);
					
					$no++; // Tambah 1 setiap kali looping
					$numrow++; // Tambah 1 setiap kali looping

				}

      // // Set width kolom
      $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); // Set width kolom A
      $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // Set width kolom B
      $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // Set width kolom C
      $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // Set width kolom D
      $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // Set width kolom E
      $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // Set width kolom E
      $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // Set width kolom E
      $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // Set width kolom E
       $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AX')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AY')->setAutoSize(true);
      $excel->getActiveSheet()->getColumnDimension('AZ')->setAutoSize(true);

      // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    
      $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
      // Set orientasi kertas jadi LANDSCAPE
      $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      // Set judul file excel nya
      $excel->getActiveSheet(0)->setTitle("Laporan Data SS");
      $excel->setActiveSheetIndex(0);
      // Proses file excel
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment; filename="Laporan Data SS.xls"'); // Set nama file excel nya
      header('Cache-Control: max-age=0');
      $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
      $write->save('php://output');
    }

    public function delete_ss($id_pertemuan)
    {
      $hariIni = new DateTime();
      $tgl  =  $hariIni->format('Y-m-d H:i:s');
      $nik = $this->session->userdata('nik');
      $data = array(
        'delete_date' => $tgl,
        'delete_by'   => $nik
      );
     
      $this->db2->where('id_pertemuan',$id_pertemuan);
      $this->db2->update('pertemuan_improvement_ss', $data);
      
      redirect('Komite','refresh');
            
	
        
	}	

  function data_department()
  {

      $json = [];
      if(!empty($this->input->get("department"))){
         $this->db1->like('department_name', $this->input->get("department"));
         
         $query = $this->db1->select('department_name')
                    ->DISTINCT()
                     ->limit(10)
                     ->get("adt_bbigroup_all_emp_new");
         $json = $query->result();
     }else{
      $this->db1->like('department_name');
      
         $query = $this->db1->select('department_name')
                      ->DISTINCT()
                     ->limit(10)
                     ->get("adt_bbigroup_all_emp_new");
      $json = $query->result();
     }
     echo json_encode($json);
  //    var_dump($json);
  }

  function data_factory()
  {

      $json = [];
      if(!empty($this->input->get("factory"))){
         $this->db1->like('factory', $this->input->get("factory"));
         
         $query = $this->db1->select('factory')
                    ->DISTINCT()
                     ->limit(10)
                     ->get("adt_bbigroup_all_emp_new");
         $json = $query->result();
     }else{
      $this->db1->like('factory');
      
         $query = $this->db1->select('factory')
                    ->DISTINCT()
                     ->limit(10)
                     ->get("adt_bbigroup_all_emp_new");
      $json = $query->result();
     }
     echo json_encode($json);
  //    var_dump($json);
  }

	function get_department_data(){

		$query = $this->db1->select('department_name')
                      ->DISTINCT()
                     ->get("adt_bbigroup_all_emp_new");
										 
		$json = $query->result();

    echo json_encode($json);
	}

  function edit_tglperbaikan()
  {
    $id_pertemuan = $this->input->post('id_pertemuan');
    $tgl = $this->input->post('meeting_date');

    $this->db2->set('meeting_date', $tgl);
    $this->db2->where('id_pertemuan', $id_pertemuan);
    $this->db2->update('pertemuan_improvement_ss');

    
    redirect('Komite/DetailPertemuanSS/'.$id_pertemuan,'refresh');
    
    
    
  }

}

/* End of file Komite.php */
