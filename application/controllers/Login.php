<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $CI = &get_instance();
        $this->db1 = $CI->load->database('default',TRUE);
    	$this->db2 = $CI->load->database('db_kedua',TRUE);
        $this->load->library('session');
        
        
    }
    
    function index()
    {
        $this->load->view('login_view');
        
    }

    public function login()
    {
        $nik = $this->input->post('nik');
        $password = $this->input->post('password');

        $where = array(
			'nik' => $nik,
			'password' => $password
            );
       
        $cek = $this->login_model->cek_login_db2("admin_data",$where)->num_rows();
        $cek2 = $this->login_model->cek_login("adt_bbigroup_emp_new",$where)->num_rows();
       // $cek3 = $this->LoginModel->cek_login("adt_bbigroup_emp_new",$where)->num_rows();
        $sql_head = "SELECT * from adt_bbigroup_all_emp_new WHERE (nik = '$nik' AND password='$password' AND position LIKE'%Head%' and status = '1') or 
        (nik = '$nik' AND password='$password' AND position LIKE'%HEAD%' and status = '1') or (nik = '170901001' AND password='$password' and status = '1') or
        (nik = '$nik' AND password='$password' AND position LIKE'%Manager%' and status = '1')";

        $cek_head = $this->db->query($sql_head)->num_rows();
        $data_head = $this->db->query($sql_head)->result();


        $sql_subdept = "SELECT * from adt_bbigroup_all_emp_new WHERE (nik = '$nik' AND password='$password' AND position LIKE'%SUB DEPT%' and status = '1') or 
        (nik = '$nik' AND password='$password' AND position LIKE'%Sub Dept%' and status = '1')";
          $cek_sub = $this->db->query($sql_subdept)->num_rows();
          $data_sub = $this->db->query($sql_subdept)->result();


        
        
       $sql_spv = "SELECT * from adt_bbigroup_all_emp_new WHERE (nik = '$nik' AND password='$password' AND position LIKE'%Super%' and status = '1') or 
        (nik = '$nik' AND password='$password' AND position LIKE '%SUPER%' and status = '1')";
       $cek_spv = $this->db->query($sql_spv)->num_rows();
       $data_spv = $this->db->query($sql_spv)->result();
    //    var_dump($data_spv['name']);
    //    die();
   
        $sql2 = "SELECT * from adt_bbigroup_all_emp_new WHERE nik = '$nik' AND password = '$password' and status = '1'";
        $cek4 = $this->db->query($sql2)->num_rows();
        $data_user = $this->db->query($sql2)->result();

        $sql_data = "SELECT * FROM admin_data WHERE nik = '$nik' AND password = '$password'";
        $cek_data = $this->db2->query($sql_data)->row();
        // var_dump($cek_head);
        // die();

       
        $sql_pass_depthead = "SELECT * from adt_bbigroup_all_emp_new where nik = '$nik' and position like '%Dept Head%' and password is null and status ='1'";
        $cek_pass_depthead = $this->db->query($sql_pass_depthead)->num_rows();
        $data_pass_depthead = $this->db->query($sql_pass_depthead)->result();
        $empty_password_depthead = strrev($nik);
       
        // kalo hasilnya 1 baris pakenya row
        // kalo hasilnya lebih dari 1 baris pakenya result

		if($cek == 1){

			
            if($cek_data->id_admin == "ADMFACTORY2")
            {
                $data_session = array(
                    'nik' => $nik,
                    'name' => $cek_data->name,
                    'status' => "login",
                    'user' => "admin_factory",
                    'factory_id' => $cek_data->factory_id
                
                
                );
     
                $this->session->set_userdata($data_session);
                redirect(base_url("AdminFactory"));
            }else{
                $data_session = array(
                    'nik' => $nik,
                    'name' => $cek_data->name,
                    'status' => "login",
                    'user' => "admin"
                
                
                    );
     
                $this->session->set_userdata($data_session);

                redirect(base_url("Komite"));
            }
            
 
            //redirect(base_url("AdminController/get_list_pertemuan/"."SS"));
 
		} else if($cek_head == 1){
            foreach($data_head as $head){
            $data_session = array(
                'nik' => $nik,
                'status' => "login",
                'name' => $head->name,
                'department_name' => $head->department_name,
                'subdept_name' => $head->subdept_name,
                'position' => $head->position,
                'factory' => $head->factory, 
                'user' => "atasan"
        
                );
            }
            $this->session->set_userdata($data_session);
            redirect(base_url("Atasan"));
           // redirect(base_url("AtasanController/list_pertemuan/SS"));
           
           
            
        }else if($cek_sub == 1){
            foreach($data_sub as $head){
            $data_session = array(
                'nik' => $nik,
                'status' => "login",
                'name' => $head->name,
                'department_name' => $head->department_name,
                'subdept_name' => $head->subdept_name,
                'position' => $head->position,
                'factory' => $head->factory, 
                'user' => "atasan"
        
                );
            }
            $this->session->set_userdata($data_session);
            redirect(base_url("Atasan"));
           // redirect(base_url("AtasanController/list_pertemuan/SS"));
           
           
            
        }
        else if($cek_spv == 1){
            foreach($data_spv as $spv){
                $data_session = array(
                    'nik' => $nik,
                    'status' => "login",
                    'name' => $spv->name,
                    'department_name' => $spv->department_name,
                    'subdept_name' => $spv->subdept_name,
                    'position' => $spv->position,
                    'factory' => $spv->factory, 
                    'user' => "spv"
            
                    );
            }
            
            
               
 
            $this->session->set_userdata($data_session);
            // var_dump($this->session->userdata('user'));
            // die();
          
            redirect(base_url("Atasan"));
            // redirect(base_url("AtasanController/list_pertemuan/SS"));
        }else if($cek4 == 1){
            foreach($data_user as $u){
            $data_session = array(
                'nik' => $nik,
                'status' => "login",
                'name' => $u->name,
                'department_name' => $u->department_name,
                'subdept_name' => $u->subdept_name,
                'position' => $u->position,
                'factory' => $u->factory, 
                'user' => "peserta"
        
                );
            }
 
            $this->session->set_userdata($data_session);
          
            
            redirect(base_url("Peserta"));
            
         //   redirect(base_url("UserController/dashboard_list"));
        }else if($cek_pass_depthead == 1){
            if($password == $empty_password_depthead)
            {
                foreach($data_pass_depthead as $head){
                    $data_session = array(
                        'nik' => $nik,
                        'status' => "login",
                        'name' => $head->name,
                        'department_name' => $head->department_name,
                        'subdept_name' => $head->subdept_name,
                        'position' => $head->position,
                        'factory' => $head->factory, 
                        'user' => "atasan"
                
                        );
                    }
                    $this->session->set_userdata($data_session);
                    redirect(base_url("Atasan"));
                    

            }else
            {
            $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert"> NIK atau password salah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('Login');
            }
        }
        else{
            
           
            $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert"> NIK atau password salah <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('Login');
            
		}
    } 

  



    function logout(){
            $this->session->sess_destroy();
            redirect(base_url('Login'));
       
		
    }

    function dashboard()
    {
        $this->load->view('dashboard');
        
    }
    
   
	


}

/* End of file Login.php */
