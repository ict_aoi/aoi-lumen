<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Daftar Registrasi Peserta </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Home</a></li>
              <li class="breadcrumb-item active">Daftar Registrasi Peserta</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
                      <div class="card-body">
                      
                        <?=$this->session->flashdata('notif')?>
                        <form class="forms-sample" action="<?= base_url('Peserta/add_regis'); ?>" method="post">
                      
                          <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label" >Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="name" name="name" value="<?php echo $this->session->userdata('name');?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Departement</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="department"  name="department" value="<?php echo $this->session->userdata('department_name');?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Sub Departement</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="subdepartment"  name="subdepartment" value="<?php echo  $this->session->userdata('subdept_name');?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Position</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="position"  name="position" value="<?php echo  $this->session->userdata('position');?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Factory</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="factory" name ="factory" value="<?php echo  $this->session->userdata('factory');?>" readonly>
                            </div>
                          </div>
                  
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Category</label>
                            <div class="col-md-9">
                          <div class="form-group">
                            <!--  <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioqcc" value="QCC" checked> QCC </label>
                            </div>
                          <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioqcp" value="QCP"> QCP </label>
                            </div> -->
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="radioss" value="SS" checked> SS </label>
                                
                            </div> 
                            
                            
                          </div>
                          </div>
                          </div>
                          
                          
                       
                      
                    
                          
                          
                     <!--   <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Foto Organisasi Team</label>
                            <input type="file" name="img[]" class="file-upload-default">
                            <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                            </span>
                            </div>
                        </div> -->

                        
                          <button type="submit" class="btn btn-success mr-2">Submit</button>
                          <button class="btn btn-light">Cancel</button>
                  
                        </form>
                      </div>
                    </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>


  