<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Beri Penilaian </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Suggestion System (SS)</a></li>
              <li class="breadcrumb-item active">Permintaan SS</li>
              <li class="breadcrumb-item active">Detail SS</li>
              <li class="breadcrumb-item active">Beri Nilai</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
                      <div class="card-body">
                     
                      <?php
                    if($cek == 1){
                      foreach($nilai as $u)
                      ?>
                      
                       <form method = "post" action = "<?= base_url('Atasan/simpan_nilai_ss/'.$u->id_pertemuan);?>">
                  
                    <input type = "hidden" name ="id_nilai" value = "<?php echo $u->id_nilai ?>"/>
                 
                   <table class="table table-striped">
                      <thead>
                        <tr>
                          <th colspan = "2"> Penilai </th>
                          <th> Nilai </th>
                         

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        <td rowspan = "3">Hasil Nyata</td>
                        <td >Penghematan Biaya/ bulan(ribu)</td>
                        <td ><input type = "number" class="form-control" name = "penghematan_biaya" value = "<?php echo $u->penghematan_biaya ?>" min = "0" max = "10"/></td>
                     
                        
                        </tr>
                        <tr>
                       
                        <td >Penghematan Man Hour /bulan</td>
                        <td ><input type = "number" class="form-control" name = "penghematan_manhour" value = "<?php echo $u->penghematan_manhour ?>" min = "0" max = "10"/></td>
                    
                    
                        </tr>
                      
                       <tr>
                       
                       <td >Safety</td>
                       <td ><input type = "number" class="form-control" name = "safety" value = "<?php echo $u->safety ?>" min = "0" max = "10"/></td>
               
                   
                       </tr>
                       <tr>
                        <td rowspan = "4">Hasil Nyata</td>
                        <td >Ergonomi</td>
                        <td ><input type = "number" class="form-control" name = "ergonomi" value = "<?php echo $u->ergonomi ?>" min = "0" max = "10"/></td>
                     
                        </tr>
                        <tr>
                       
                       <td >Quality</td>
                       <td ><input type = "number" class="form-control" name = "quality" value = "<?php echo $u->quality ?>" min = "0" max = "10"/></td>
                       
                   
                       </tr>
                       <tr>
                       
                       <td >Manfaat</td>
                       <td ><input type = "number" class="form-control" name = "manfaat" value = "<?php echo $u->manfaat ?>" min = "0" max = "10"/></td>
                     
                   
                       </tr>
                       <tr>
                       
                       <td >Keaslian</td>
                       <td ><input type = "number" class="form-control" name = "keaslian" value = "<?php echo $u->keaslian ?>" min = "0" max = "10"/></td>
                    
                   
                       </tr>
                      
                        <td rowspan = "2">Aspek Umum</td>
                       <td >Usaha</td>
                       <td ><input type = "number" class="form-control" name = "usaha" value = "<?php echo $u->usaha ?>" min = "0" max = "10"/></td>
                  
                   
                       </tr>
                       <tr>
                       
                       <th>Total</th>
                       <th ><?php echo $u->total_nilai ?></th>
                  
                   
                       </tr>
                      
                     
                      </tbody> 
                    </table>
             
                            <button type="submit" class="btn btn-info mr-2">Save</button>
                     
                   </form>
                      <?php
                    } else{

                    
                    
                    ?>
                  <form method = "post" action = "<?= base_url('Atasan/simpan_nilai_ss/'.$id_pertemuan);?>">
                 
                 <table class="table table-striped">
                    <thead>
                      <tr>
                        <th colspan = "2"> Penilai </th>
                        <th> Nilai </th>
                       

                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                      <td rowspan = "3">Hasil Nyata</td>
                      <td >Penghematan Biaya/ bulan(ribu)</td>
                      <td ><input type = "number" class="form-control" name = "penghematan_biaya" value = "0" min = "0" max = "10"/></td>
                   
                      
                      </tr>
                      <tr>
                     
                      <td >Penghematan Man Hour /bulan</td>
                      <td ><input type = "number" class="form-control" name = "penghematan_manhour" value = "0" min = "0" max = "10"/></td>
                  
                  
                      </tr>
                   
                     <tr>
                     
                     <td >Safety</td>
                     <td ><input type = "number" class="form-control" name = "safety"  value = "0" min = "0" max = "10"/></td>
             
                 
                     </tr>
                     <tr>
                      <td rowspan = "4">Hasil Nyata</td>
                      <td >Ergonomi</td>
                      <td ><input type = "number" class="form-control" name = "ergonomi" value = "0" min = "0" max = "10"/></td>
                   
                      </tr>
                      <tr>
                     
                     <td >Kualitas</td>
                     <td ><input type = "number" class="form-control" name = "quality" value = "0" min = "0" max = "10"/></td>
                     
                 
                     </tr>
                     <tr>
                     
                     <td >Manfaat</td>
                     <td ><input type = "number" class="form-control" name = "manfaat" value = "0" min = "0" max = "10"/></td>
                   
                 
                     </tr>
                     <tr>
                     
                     <td >Keaslian</td>
                     <td ><input type = "number" class="form-control" name = "keaslian"  value = "0" min = "0" max = "10"/></td>
                  
                 
                     </tr>
                   
                      <tr>
                      <td rowspan = "2">Aspek Umum</td>
                     <td >Usaha</td>
                     <td ><input type = "number" class="form-control" name = "usaha" value = "0" min = "0" max = "10"/></td>
                      
                 
                     </tr>
                    
                   
                    </tbody> 
                  </table>
               
           
                          <button type="submit" class="btn btn-info mr-2">Save</button>
                   
                 </form>
                    <?php
                    } ?>
                  
                        
                       
                      </div>
                    
                    </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>

