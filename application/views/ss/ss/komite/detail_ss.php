<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail Pertemuan </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Suggestion System (SS)</a></li>
              <li class="breadcrumb-item active">Daftar SS</li>
              <li class="breadcrumb-item active">Detail SS</li>
 
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
        <div class="card-body">
        
                    <form class="form-sample">
                      <?php
                     
                      foreach($detail as $u){

                      
                      ?>
                    
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">NIK</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value="<?php echo $u->nik?> " readonly/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value="<?php echo $u->nama_peserta?> " readonly />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Department</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $u->department_name?> " readonly/>
                            </div>
                          </div>
                        </div>
                       
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Sub Department</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $u->subdept_name?> " readonly/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Position</label>
                            <div class="col-sm-9">
                              <input class="form-control" value="<?php echo $u->position?> " readonly />
                            </div>
                          </div>
                        </div>

												<!-- Resign Status -->
												<div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
															<?php if ($u->status_karyawan == 1){
																?>
                              		<input class="form-control" value="Active" readonly>
															<?php }else{
																?>
                              		<input class="form-control" value="Inactive" readonly>
															<?php }	
															?>
                            </div>
                          </div>
                        </div>
                      </div>
                     
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Judul</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $u->judul?>" readonly />
                            </div>
                          </div>
                        </div>
                        
                      
                      </div>

                  
                      
                   

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Uraian Masalah</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->uraian_masalah?> </textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Keadaan Seharusnya</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->keadaan_seharusnya?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>
                       

                      
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Usulan Perbaikan</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->usulan_perbaikan?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Biaya Kerugian</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->biaya_kerugian?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Tanggal Perbaikan</label>
                            <?php
                            if($u->approve_komite == '2'){

                              ?>
                               <div class="col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo date("d-m-Y", strtotime($u->meeting_date));?>" readonly>
                           </div>
                              
                              <?php
                            }else{
                            
                            ?>
                             <div class="col-sm-7">
                            <input type="text" class="form-control" value = "<?php echo date("d-m-Y", strtotime($u->meeting_date));?>" readonly>
                           </div>
                           
                            <div class="col-sm-2">
                            <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modal-default<?php echo $u->id_pertemuan. $u->meeting_date?>">  <i class="fas fa-edit">Ubah</i></a>
                           </div>
                           <?php
                            }?>
                           
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Waktu Perbaikan</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo $u->meeting_time?> " readonly>
                            </div>
                          </div>
                        </div>
                        </div>
                        <br>
                        <h4><b>Laporan Perbaikan</h4><br>
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Aktivitas Perbaikan</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->aktivitas_perbaikan?> </textarea>
                            </div>
                          </div>
                        </div>

                        </div>
  
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Ilustrasi Produk<br> Before</label>
                            <div class="col-sm-9">
                            <?php
                              $location=base_url().'upload/';
                              if($u->produk_before != null || $u->produk_before != ""){
                                ?>
                                <img src = "<?php echo $location.$u->produk_before; ?>" alt= "image" width="45%"  />
                                <?php
                              } else{
                                ?>
                                <label class=" col-form-label" >(Ilustrasi Produk belum ada) </label>
                                <?php
                              }

                              ?>
                            <!-- <img src = "<?php echo $location.$u->produk_before; ?>" alt= "image" width="45%"  /> -->
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Ilustrasi Produk<br> After</label>
                            <div class="col-sm-9">
                            <?php
                              $location=base_url().'upload/';
                              if($u->produk_after != null || $u->produk_after != ""){
                                ?>
                                <img src = "<?php echo $location.$u->produk_after; ?>" alt= "image" width="45%"  />
                                <?php
                              } else{
                                ?>
                                <label class=" col-form-label" >(Ilustrasi Produk belum ada) </label>
                                <?php
                              }

                              ?>
                           </div>
                          </div>
                        </div>
                      </div>

                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Quality <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->quality_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Quality <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->quality_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>

                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Cost <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->cost_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Cost <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->cost_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>


                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Delivery <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->delivery_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Delivery <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->delivery_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>
                    
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Safety <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->safety_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Safety <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->safety_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>

                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Moral <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->moral_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Moral <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->moral_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>

                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Productivity <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->productivity_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Productivity <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->productivity_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>
                       
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Environtment <br>Before</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->environtment_before?> </textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label" >Environtment <br>After</label>
                            <div class="col-sm-9">
                            <textarea type="text" class="form-control" readonly><?php echo $u->environtment_after?> </textarea>
                            </div>
                          </div>
                        </div>
                        </div>
                       
                       
                        
                     
                   
                    
                    </form>

                    
                  </div>
                  

                  <div class="card-body">

                
                        <h4 class="card-title">Atasan</h4>
                        <br>
                    
                        <form class="forms-sample">
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Supervisor</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $u->nik_spv?>" readonly />
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                           
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo $u->nama_supervisor?> " readonly />
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Sub Dept. Head</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $u->nik_subdept_head?>" readonly/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                           
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo $u->nama_subdept_head?> " readonly />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Dept. Head</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $u->nik_dept_head?>" readonly />
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                           
                            <div class="col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo $u->nama_dept_head?> " readonly/>
                            </div>
                          </div>
                        </div>
                      </div>
                     
                   
                        </form>
                        <?php
                    $nik = $this->session->userdata('nik');
               
                    if($u->approve_komite == 0){
                      ?>
                      <a href = "<?= base_url('Komite/approve_permintaan_ss/'. $u->id_pertemuan); ?>"
                      class="btn btn-success mr-2"> Approve
                                        <!-- <button type="submit" class="btn btn-success mr-2">Approve</button> -->
                                        </a>
                                        <a href = "<?= base_url('AdminController/reject_permintaan_ss/'. $u->id_pertemuan); ?>"
                                        class="btn btn-danger mr-2"> Reject
                                        <!-- <button type="submit" class="btn btn-danger mr-2">Reject</button> -->
                                        </a>
                      <?php
                    } else if($u->approve_komite == 1){
                      ?>
                        <a href = "<?= base_url('Komite/dashboard_formpenilaian/'.$u->id_pertemuan); ?>"
                        class="btn btn-success mr-2"> Beri Penilaian
                        <!-- <button type="submit" class="btn btn-success mr-2">Beri Penilaian</button> -->
                        </a>
                        <a href = "<?= base_url('Komite/print/'.$u->id_pertemuan); ?>" class="btn btn-success mr-2"> Print SS
                            <!-- <button type="submit" class="btn btn-success mr-2">Print SS</button> -->
                            </a>
                      <?php
                    }
                
                     
                    

                    ?>
                    
                        <?php
                      }?>
                     
                  </div>
                
                  

                  <div class="card-body">
                  
                    <h4 class="card-title">Penilaian</h4>
                  
                      <table class="table table-striped">
                  
                      <thead>
                        <tr>
                          <th > Penilai </th>
                          <th> Supervisor </th>
                          <th> Sub Dept Head </th>
                          <th> Dept Head </th>
                          <th> Komite </th>
                       

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                     
                        <td >Penghematan Biaya/ bulan(ribu)</td>
                        <td ><?php  foreach ($nilai_spv as $n) { echo $n->penghematan_biaya; }?></td>
                        <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->penghematan_biaya; }?></td>
                        <td ><?php  foreach ($nilai_dept as $dept) { echo $dept->penghematan_biaya; }?></td>
                        <td ><?php   foreach ($nilai_komite as $komite) {echo $komite->penghematan_biaya; }?></td>
                       
                        <td ></td>
                        </tr>
                        <tr>
                       
                        <td >Penghematan Man Hour /bulan</td>
                        <td ><?php  foreach ($nilai_spv as $n) { echo $n->penghematan_manhour; }?></td>
                        <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->penghematan_manhour; }?></td>
                        <td ><?php  foreach ($nilai_dept as $dept) { echo $dept->penghematan_manhour; }?></td>
                        <td ><?php   foreach ($nilai_komite as $komite) { echo $komite->penghematan_manhour; }?></td>
                       
                       
                        </tr>
                        <tr>
                       
                       <td >Quality</td>
                       <td ><?php  foreach ($nilai_spv as $n) { echo $n->quality; }?></td>
                       <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->quality;}?></td>
                       <td ><?php  foreach ($nilai_dept as $dept) { echo $dept->quality; }?></td>
                       <td ><?php    foreach ($nilai_komite as $komite) {echo $komite->quality; }?></td>
                      
                       
                       </tr>
                       <tr>
                       
                       <td >Safety</td>
                       <td ><?php  foreach ($nilai_spv as $n) { echo $n->safety; }?></td>
                       <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->safety; }?></td>
                       <td ><?php foreach ($nilai_dept as $dept) { echo $dept->safety; }?></td>
                       <td ><?php   foreach ($nilai_komite as $komite) { echo $komite->safety; }?></td>
                       </tr>
                       <tr>
                      
                        <td >Ergonomi</td>
                        <td ><?php  foreach ($nilai_spv as $n) { echo $n->ergonomi; }?></td>
                        <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->ergonomi; }?></td>
                        <td ><?php foreach ($nilai_dept as $dept) { echo $dept->ergonomi; }?></td>
                        <td ><?php   foreach ($nilai_komite as $komite) { echo $komite->ergonomi; }?></td>
             
                       
                        </tr>
                     
                       <tr>
                       
                       <td >Manfaat</td>
                       <td ><?php  foreach ($nilai_spv as $n) { echo $n->manfaat; } ?></td>
                       <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->manfaat; }?></td>
                       <td ><?php foreach ($nilai_dept as $dept) { echo $dept->manfaat; }?></td>
                       <td ><?php   foreach ($nilai_komite as $komite) { echo $komite->manfaat; }?></td>
               
                       
                       </tr>
                       <tr>
                       
                       <td >Keaslian</td>
                       <td ><?php  foreach ($nilai_spv as $n) { echo $n->keaslian; } ?></td>
                       <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->keaslian; }?></td>
                       <td ><?php foreach ($nilai_dept as $dept) { echo $dept->keaslian;}?></td>
                       <td ><?php   foreach ($nilai_komite as $komite) { echo $komite->keaslian; } ?></td>
          
                       
                       </tr>
                       
                        <tr>
                       
                       <td >Usaha</td>
                       <td ><?php  foreach ($nilai_spv as $n) { echo $n->usaha; }?></td>
                       <td ><?php  foreach ($nilai_subdept as $sub) { echo $sub->usaha;} ?></td>
                       <td ><?php  foreach ($nilai_dept as $dept) { echo $dept->usaha; } ?></td>
                       <td ><?php   foreach ($nilai_komite as $komite) { echo $komite->usaha; }?></td>
                  
                       </tr>
                       <tr>
                          <th> Total </th>
                          <th> <?php  foreach ($nilai_spv as $n) { echo $n->total_nilai; }?> </th>
                          <th> <?php  foreach ($nilai_subdept as $sub) { echo $sub->total_nilai; }?>  </th>
                          <th> <?php  foreach ($nilai_dept as $dept) { echo $dept->total_nilai; }?>  </th>
                          <th><?php   foreach ($nilai_komite as $komite) { echo $komite->total_nilai; }?>   </th>
                  
                        </tr>
                        <tr> 
                          <th>   </th>
                          <th > <?php  foreach ($nilai_spv as $n) { if( $n->tanggal_penilaian != null){
                                echo "Diisi pada " .  date("d-m-Y", strtotime($n->tanggal_penilaian));
                          } else{
                            echo "-";
                          }
                           }?> </th>
                          <th> <?php  foreach ($nilai_subdept as $sub) { if( $sub->tanggal_penilaian != null){
                                echo "Diisi pada " .  date("d-m-Y", strtotime($sub->tanggal_penilaian));
                          } else{
                            echo "-";
                          }
                             }?>  </th>
                          <th> <?php  foreach ($nilai_dept as $dept) { 
                            if(  $dept->tanggal_penilaian != null){
                              echo "Diisi pada " .   date("d-m-Y", strtotime($dept->tanggal_penilaian));
                        } else{
                          echo "-";
                        } }?>  </th>
                          <th><?php   foreach ($nilai_komite as $komite) {  if(  $komite->tanggal_penilaian != null){
                              echo "Diisi pada " .   date("d-m-Y", strtotime($komite->tanggal_penilaian));
                        } else{
                          echo "-";
                        }
                           }?>   </th>
                  
                        </tr>
                     
                      </tbody> 
                         <?php
                 
                 
                   ?>
                    </table>
                   
                   
              
                </div>

    </div>         
     </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>

  <div class="modal fade" id="modal-default<?php echo $u->id_pertemuan.$u->meeting_date; ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Tanggal Perbaikan </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?= base_url('Komite/edit_tglperbaikan'); ?>" method="post">
              <div class="form-group row">
              <input type="hidden" class="form-control" id = "id_pertemuan" name = "id_pertemuan" value = "<?php echo $u->id_pertemuan; ?>">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label" required>Tanggal</label>
                            <div class="col-sm-9">
                              <input type="date" class="form-control" id="meeting_date"  name="meeting_date" value = "<?php echo $u->meeting_date; ?>">
                            </div>
                          </div>
             
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

