
<html>

<table style=" width:100%;">
<tr>
<th><img src = "<?= base_url('assets/img/Logo.png'); ?>" alt= "image" width = "20%" /> </th>
<th><h3>FORM SUGGESTION SYSTEM / IDE PERBAIKAN</th>
</tr>

<tr>
<td colspan = "2"> 


<table style = "padding:10px;width:100%;">
<?php
foreach ($pertemuan as $p) {
  ?>
<tr >
<td >NO REGISTER SS  </td>
<td> : </td>
<td> <?php echo $p->no_register; ?> </td>
</tr>
<tr>
<td>Judul / Tema </td>
<td> : </td>
<td style = "100%"><?php echo $p->judul; ?> </td>
</tr>
<tr>
<td>Dibuat Oleh </td>
<td> : </td>
<td> <?php echo $p->name . ' (' .  $p->nik . ')' ?></td>

</tr>


<tr>
<td>Bagian / Line </td>
<td> : </td>
<td> <?php echo $p->department_name; ?></td>

</tr>
<tr>
<td>Jabatan </td>
<td> : </td>
<td> <?php echo $p->position; ?></td>
</tr>

<tr>
<td>Lokasi Perbaikan </td>
<td> : </td>
<td> <?php echo $p->location; ?></td>
</tr>

<tr>
<td>Category</td>
<td> : </td>
<td> <?php echo $p->category_percategory;?></td>
</tr>

</table>

</td>
</th>
</tr>


<tr>
<td colspan = "2">
<table style="border-collapse:collapse; border:1px solid black;width:100%">
<tr>
<td style = " border:1px solid black;" width = "348px">Uraian Masalah</td>
<td  style = "border:1px solid black; " width = "348px">Biaya Kerugian</td>
</tr>
<tr>
<td  style = " border:1px solid black; width:50%"> 
<p> <?php echo $p->uraian_masalah; ?></p>
</td>
<td>
<p> <?php echo $p->biaya_kerugian; ?></p>
</td>

</tr>
<tr>
<td style = " border:1px solid black; "width = "348px">Keadaan Seharusnya</td>
<td  style = "border:1px solid black; " width = "348px">Usulan Perbaikan</td>
</tr>
<tr>
<td  style = " border:1px solid black; width:50%"> 
<p> <?php echo $p->keadaan_seharusnya; ?></p>
</td>
<td  style = " border:1px solid black; width:50%">

<p> <?php echo $p->usulan_perbaikan; ?></p>
</td>
</tr>
<?php
}
foreach ($laporan as $l) {

?>
<tr>
<th colspan = "2">Laporan Perbaikan</th>
</tr>

<tr>
<td style = " border:1px solid black;" width = "348px">Ilustrasi Produk (Before)</td>
<td  style = "border:1px solid black; " width = "348px">Ilustrasi Produk (After)</td>
</tr>
<tr>
<td style = "border:1px solid black; width:50%; height: 25%; text-align:center; " >
<?php
                                $location=base_url().'upload/';
                                if($l->produk_before != null || $l->produk_before != ""){
                                    ?>
                                    <img src = "<?php echo $location.$l->produk_before; ?>" alt= "image" width="45%" align = "center" />
                                    <?php
                                } else{
                                    ?>
                                    <label>(Ilustrasi Produk belum ada) </label>
                                    <?php
                                }

                              ?>

</td>
<td style = "width:50%; text-align:center; ">

<?php
                              $location=base_url().'upload/';
                              if($l->produk_after != null || $l->produk_after != ""){
                                ?>
                                <img src = "<?php echo $location.$l->produk_after; ?>" alt= "image" width="45%"  />
                                <?php
                              } else{
                                ?>
                                <label class=" col-form-label" >(Ilustrasi Produk belum ada) </label>
                                <?php
                              }

                              ?>
</td>
</tr>

<tr>
<td colspan = "2" style = " border:1px solid black; width:50%">Aktivitas Perbaikan</td>
</tr>
<tr>
<td colspan = "2">
<p> <?php if($l->aktivitas_perbaikan != NULL){
    echo $l->aktivitas_perbaikan;
}
else{
    echo " - ";
} ?></p>
</td>

</tr>
</table>
</td> </tr>
<br>



<tr>
<td colspan = "2">
<table style = "border-collapse:collapse; border:1px solid black;" width = "696px">
<tr >
<th style = "border-collapse:collapse; border:1px solid black;" width = "220px">Faktor </th>
<th style = "border-collapse:collapse; border:1px solid black;" width = "232">Before </th>
<th style = "border-collapse:collapse; border:1px solid black;"  width = "232">After </th>
</tr>

<tr>
<td style = "border-collapse:collapse; border:1px solid black;"> Quality </td>
<td style = "border-collapse:collapse; border:1px solid black;"> <?php echo $l->quality_before; ?> </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->quality_after; ?>  </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;"> Cost </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->cost_before; ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->cost_after; ?>  </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;"> Delivery </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->delivery_before; ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->delivery_after; ?>  </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;"> Safety </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->safety_before; ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->safety_after; ?>  </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;"> Moral </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->moral_before; ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black;"> <?php echo $l->moral_after; ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;"> Productivity </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->productivity_before; ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black;"> <?php echo $l->productivity_after; ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black; "> Environtment </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->environtment_before; ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black;"><?php echo $l->environtment_after; ?>  </td>
</tr>
</table>
</td>
</tr><br>

<tr>
<td colspan = "2">
<table style="border-collapse:collapse; border:1px solid black;width:100%">
<tr>
<td style = " border:1px solid black; width:50%">Biaya Improvement</td>

</tr>
<tr>
<td style = " border:1px solid black; width:50%"> 
<p><?php if($l->biaya_imp != NULL){
echo $l->biaya_imp; 
}else{
    echo "-";
}
?> </p>
</td>

</tr>
<tr>
<td  style = "border:1px solid black;  width:50%">Perhitungan Benefit</td>
</tr>
<tr>
<td style = " border:1px solid black; width:50%">
<p><?php if($l->perhitungan_benefit != NULL){
echo $l->perhitungan_benefit; 
}else{
    echo "-";
} ?> </p>
<p><b>Total Benefit : </b> <?php if($l->total_benefit != NULL){
    $hasil_rupiah = "Rp " . number_format($l->total_benefit,2,',','.');
echo $hasil_rupiah; 
}else{
    echo "-";
} ?></p>
</td>
</tr>

<?php
} ?>
</table>
</td> </tr>

<tr>
<td colspan = "2">
<?php

   
     
       

?>
<table style = "border-collapse:collapse; border:1px solid black;width:100%">

<tr >
<th style = "border-collapse:collapse; border:1px solid black;">Penilaian </th>
<th style = "border-collapse:collapse; border:1px solid black;">SPV </th>
<th style = "border-collapse:collapse; border:1px solid black;">Sub dept Head </th>
<th style = "border-collapse:collapse; border:1px solid black;">Dept Head </th>
<th style = "border-collapse:collapse; border:1px solid black;">Komite </th>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Penghematan Biaya /bulan (ribu) </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->penghematan_biaya; } ?></td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_subdept as $subdept){ echo $subdept->penghematan_biaya; }?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php    foreach($nilai_depthead as $depthead){ echo $depthead->penghematan_biaya; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center; ">
<?php     foreach($nilai_komite as $komite){ echo $komite->penghematan_biaya; } ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Penghematan Man Hour /bulan </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->penghematan_manhour; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->penghematan_manhour;} ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_depthead as $depthead){ echo $depthead->penghematan_manhour; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php     foreach($nilai_komite as $komite){ echo $komite->penghematan_manhour; } ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Quality  </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->quality; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->quality; } ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php    foreach($nilai_depthead as $depthead){ echo $depthead->quality; } ?>  </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php     foreach($nilai_komite as $komite){ echo $komite->quality; } ?>  </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Safety </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->safety; } ?></td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->safety; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php    foreach($nilai_depthead as $depthead){ echo $depthead->safety; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php        foreach($nilai_komite as $komite){ echo $komite->safety; } ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Ergonomi </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->ergonomi; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->ergonomi; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_depthead as $depthead){ echo $depthead->ergonomi; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_komite as $komite){ echo $komite->ergonomi; } ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Manfaat </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->manfaat; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->manfaat; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_depthead as $depthead){ echo $depthead->manfaat; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php     foreach($nilai_komite as $komite){ echo $komite->manfaat; } ?> </td>
</tr>

<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Keaslian </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->keaslian; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->keaslian; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php    foreach($nilai_depthead as $depthead){ echo $depthead->keaslian; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
 <?php     foreach($nilai_komite as $komite){ echo $komite->keaslian; } ?></td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;">Usaha </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_spv as $spv){ echo $spv->usaha; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->usaha; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php   foreach($nilai_depthead as $depthead){ echo $depthead->usaha; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;">
<?php foreach($nilai_komite as $komite){echo $komite->usaha; } ?> </td>
</tr>
<tr>
<td style = "border-collapse:collapse; border:1px solid black;"><b>Total </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;"><b>
<?php foreach($nilai_spv as $spv){ echo $spv->total_nilai; } ?></td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;"><b>
<?php  foreach($nilai_subdept as $subdept){ echo $subdept->total_nilai; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;"><b>
<?php foreach($nilai_depthead as $depthead){ echo $depthead->total_nilai; } ?> </td>
<td style = "border-collapse:collapse; border:1px solid black; text-align:center;"><b>
<?php foreach($nilai_komite as $komite){ echo $komite->total_nilai;} ?> </td>
</tr>

</table>

</td>
</tr>

<tr>
<td colspan = "2">
<table style = "border:1px solid black; width:100%; text-align:center;">
<tr>
<th>Nilai</th>
<th>Hadiah </th>
<th>Persetujuan Akhir </th>
</tr>
<tr>
<td> <?php foreach($nilai_komite as $komite){ echo $komite->total_nilai;} ?></td>
<td><?php foreach($nilai_komite as $k){
 
        if($k->total_nilai >= '0' && $k <= '27'){
            echo "Rp 0";
        } else if($k->total_nilai >= '28' && $k->total_nilai <= '40'){
            echo "Rp 2.000";
        } else if($k->total_nilai >= '41' && $k->total_nilai <= '50'){
            echo "Rp 5.000";
        } else if($k->total_nilai >= '51' && $k->total_nilai <= '65'){
            echo "Rp 10.000";
        } else if($k->total_nilai >= '66' && $k->total_nilai <= '75'){
            echo "Rp 15.000";
        } else if($k->total_nilai >= '76' && $k->total_nilai < '81'){
            echo "Rp 20.000";
        } else{
            echo "-";
        }
    } 
 ?> </td>
<td><?php foreach ($nilai_komite as $k) {
    if($k->reward == "Y"){
        echo "Reward";
    } else if($k->reward == "N"){
        echo "Tidak ada reward";
    }else{
        echo "-";

    }
   
} ?> </td>
</tr>

</table><br><br>
</td>
</tr>

<tr>
<td>KLASIFIKASI HADIAH</td>
</tr>

<tr>
<td  colspan = "2"> 

<table style = "border-collapse:collapse; border:1px solid black;width:100%; text-align:center;">
<tr>
<th style = "border:1px solid black;">Nilai</th>
<th style = "border:1px solid black;">0-27</th>
<th style = "border:1px solid black;">28-40</th>
<th style = "border:1px solid black;">41-50</th>
<th style = "border:1px solid black;">51-65</th>
<th style = "border:1px solid black;">66-75</th>
<th style = "border:1px solid black;">76-80</th>
</tr>
<tr>
<td style = "border:1px solid black;">Rangking</td>
<td style = "border:1px solid black;">6</td>
<td style = "border:1px solid black;">5</td>
<td style = "border:1px solid black;">4</td>
<td style = "border:1px solid black;">3</td>
<td style = "border:1px solid black;">2</td>
<td style = "border:1px solid black;">1</td>
</tr>
<tr>
<td style = "border:1px solid black;">Hadiah</td>
<td style = "border:1px solid black;">Rp 0(hanya nilai KPI)</td>
<td style = "border:1px solid black;">Rp 2.000</td>
<td style = "border:1px solid black;">Rp 5.000</td>
<td style = "border:1px solid black;">Rp 10.000</td>
<td style = "border:1px solid black;">Rp 15.000</td>
<td style = "border:1px solid black;">Rp 20.000</td>
</tr>
<tr>
<td style = "border:1px solid black;">Penilai</td>
<td colspan = "4" style = "border:1px solid black;">GL s/d Sub.Dept & Committe</td>
<td colspan = "2" style = "border:1px solid black;">FM/ Div. Head & Committe</td>

</tr>



</table>
</td>
</tr>

<tr>
<td  colspan = "2">
<br><br><br>
<table style = "width:100%; text-align:center;">

<tr>
<th>Supervisor</th>
<th>Sub Dept Head</th>
<th>Dept Head</th>
<th>Komite</th>
</tr>
<tr>
<td> <br>
<?php foreach($pertemuan as $p){
 if($p->approve_spv == 1){
    echo "Approved";
} else if($p->approve_spv == 0){
    echo "Belum di approve";
} else{
    echo "Ditolak";
} }?><br><br><br>
( <?php  foreach($nama_spv as $s){
 if(!empty($s->name)){
    echo $s->name;
} else{
    echo "-";
} }
 ?> )</td>
<td> <br>
<?php foreach($pertemuan as $p){
 if($p->approve_subdept_head == 1){
    echo "Approved";
} else if($p->approve_subdept_head == 0){
    echo "Belum di approve";
} else{
    echo "Ditolak";
} }?><br><br><br>
( <?php  foreach($nama_subdept as $h){ 
    if(!empty($h->name)){
        echo $h->name;
    } else{
        echo "-";
    }
 } ?> )</td>
<td><br>
<?php foreach($pertemuan as $p){
if($p->approve_dept_head == 1){
    echo "Approved";
} else if($p->approve_dept_head == 0){
    echo "Belum di approve";
} else{
    echo "Ditolak";
} }?> <br><br><br>
( <?php   foreach($nama_dept as $h){ 
    if(!empty($h->name)){
        echo $h->name;
    } else{
        echo "-";
    }
 } ?> )</td>
<td><br>
<?php foreach($pertemuan as $p){
if($p->approve_komite== 1){
    echo "Approved";
} else if($p->approve_komite == 0){
    echo "Belum di approve";
} else{
    echo "Ditolak";
} }?><br><br><br>
( <?php   foreach($nama_komite as $k){ 
    
    if(!empty($k->name)){
        echo $k->name;
    } else{
        echo "-";
    }
 } ?> )</td>
</tr>
</table>
</td>
</tr>




</table>

</html>
