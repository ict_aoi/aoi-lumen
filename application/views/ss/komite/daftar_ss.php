<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pertemuan Improvement </h1><br>
            <!-- <a href = "<?= base_url('Komite/export'); ?>"> -->
            <!-- </a> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Suggestion System (SS)</a></li>
              <li class="breadcrumb-item active">Permintaan SS</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

		<!-- Filter  -->

		<div class="content">

			<div class="row">

				<div class="col-12">
					<div class="card">
						
							<div class="card-body">
              
                <div class="row">

									
                      <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal-tarik">Download Report</button>


                      </div>
                      <br>
                <div class = "row">
                <h6 class="m-0 text-dark">Filter Pencarian</h6>
                <br>
                </div>
               
								<div class="row">

									
								
							<!-- Tanggal Mulai -->
              <input type="date" id="filterBulan" name="filterBulan" class="btn" style="border: 1px solid black;">
              <p>&nbsp &nbsp &nbsp</p>

                <!-- Tanggal Selesai -->
                <input type="date" id="filterBulanAkhir" name="filterBulanAkhir" class="btn" style="border: 1px solid black;">
                  <p>&nbsp &nbsp &nbsp</p>
									
									<select name="filterFactory" id="filterFactory" class="form-control col-3">
										
										<option value="" selected hidden>-- Pilih Factory --</option>
										<option value="BBIS">BBIS</option>
										<option value="AOI1">AOI1</option>
										<option value="AOI2">AOI2</option>
										
									</select>
                  <p>&nbsp &nbsp &nbsp</p>

									<select name="filterDept" id="filterDept" class="form-control col-3">
										
									</select>
                  <br>
								
								</div>
                <br>
                <div class = "row">
                <button id="resetFilter" class="btn btn-warning">Reset</button>
                  <p>&nbsp &nbsp </p>
									<button id="cariFilter" class="btn btn-success">Cari</button>
									</div>
               
							</div>

					</div>
				</div>

			</div>

		</div>

		<!-- End Filter -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
          
            <!-- /.card-header -->
            <div class="card-body">
            

                    <input type = "hidden" value = "SS" id= "category" name= "category"/>
                    <div class="table-responsive">
                      <table id = "table-pertemuan" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%"  >
                        <thead>
                          <tr>
                          <th> No </th>
                          <th> Name </th>
                          <th> Judul</th>
                          <th> Tgl <br> Dibuat </th>
                          <th> Tgl <br> Perbaikan </th>
                          <th> Dept</th>
                          <th> Factory </th>
                          <th> App<br>SPV </th>
                          <th> App Sub<br> Dept.Head </th>
                          <th> App <br> Dept. Head </th>
                          <th> App <br> Komite </th>
                          <th> Detail </th>
                         
                          
                          </tr>
                        </thead>
                        <tbody>
                     
                        </tbody> 
                      </table>
                    </div>
                  </div>
            <!-- /.card-body -->
          </div>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>
  <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Notifikasi</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Apakah anda yakin ingin menghapus ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- delete_ss -->
              <a href = "#" class="btn btn-primary">Delete </a>
              <!-- <button type="button" class="btn btn-primary">Delete</button> -->
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      <div class="modal fade" id="modal-tarik">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Download Report</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action = "<?= base_url('Komite/export');?>" method = "post"  class="forms-sample" >
              <!-- <div class="col-md-6">
              <div class="form-group row">
                        
                        <label class="col-sm-3 col-form-label">Select Department</label>
                        <div class="col-sm-9">
                        <select class="form-control departement" name="departement" id = "departement" class="form-control">
                        </select>
                           
                        </div>
                    </div>
              </div> -->

              <div class="form-group">
                  <label>Pilih Department</label>
                  <select class="form-control departement" name="department" id = "department" class="form-control" style="width: 100%;">
                        </select>
                
                </div>
                <div class="form-group">
                  <label>Pilih Factory</label>
                  <select class="form-control factory" name="factory" id = "factory" class="form-control" style="width: 100%;">
                        </select>
                
                </div>
                <div class="form-group">
                  <label>Pilih Tanggal Awal dan Akhir</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                  
                    <input type="date" class="form-control float-right" name = "tgl_awal"  required>
                    
                    <input type="date" class="form-control float-right" name = "tgl_akhir" required>
                  </div>
                
                </div>
                <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Download</button> 
            </div>
              </form>
              </form>
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

  <!-- <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script> -->
  <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css"> -->
  
  <!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script> -->
	<link href="<?= base_url('assets/plugins/datatables/jquery.dataTables.css');?>" rel="stylesheet" />
  <script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.js');?>"></script>
	
  <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.min.js');?>"></script>
<!-- Toastr -->
  <script src="<?= base_url('assets/plugins/toastr/toastr.min.js');?>"></script>
  
<link href="<?= base_url('assets/plugins/select2/css/select2.min.css');?>" rel="stylesheet" />
<link href="<?= base_url('assets/plugins/select2/css/select2.css');?>" rel="stylesheet" />
<link href="<?= base_url('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');?>" rel="stylesheet" />
<script src="<?= base_url('assets/plugins/select2/js/select2.min.js');?>"></script>

<script src="<?= base_url('assets/plugins/daterangepicker/daterangepicker.js');?>"></script>
<script type="text/javascript">
     
     $(document).ready(function () {
 
         var table =$('#table-pertemuan').DataTable({
             "processing": true,
             "serverSide": true,
             // "order": [],
             "orderMulti"  : true,
             "ajax":{
             "url": "<?php echo base_url();?>Komite/list_pertemuan",
             "dataType": "json",
             "type": "POST",
             "data":function(data) {                         
                 //    data.category = $('#category').val();
                 data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
 
             }},
           "columns": [
                 { "data" : null, 'sortable' : false},
                 { "data": "name" },
                 { "data": "judul" },
                 { "data": "date_created" },
                 { "data": "meeting_date" },
                 { "data": "department_name" },
                 { "data": "factory" },
                 { "data": "approve_spv" },
                 { "data": "approve_subdept_head" },
                 { "data": "approve_dept_head" },
                 { "data": "approve_komite" },  
                 { "data": "action",'sortable' : false },
             ],
             fnCreatedRow: function (row, data, index) { 
               var info = table.page.info(); 
               var value = index+1+info.start; 
               $('td', row).eq(0).html(value); 
             }
         });
 
         // Set Data di departmen
         $(function(){
           $.ajax({
             url: 'Komite/get_department_data',
             dataType: 'json',
             success: function(data){
               console.log('Success',data);
               var list = $('#filterDept');
               list.empty();
               list.append('<option value="" selected hidden>-- Pilih Department --</option>');
               for(var i = 0; i < data.length;i++){
                 list.append('<option>'+ data[i].department_name +'</option>');
               }
             },
             error: function(data){
               console.log('Error',data);
             }
           })
         })
 
         $('#table-pertemuan_filter input').unbind();
         $('#table-pertemuan_filter input').bind('keyup', function(e) {
             if (e.keyCode == 13 || $(this).val().length == 0 ) {
                 table.search($(this).val()).draw();
             }
              // if ($(this).val().length == 0 || $(this).val().length >= 3) {
             //     table.search($(this).val()).draw();
             // }
         });
         $('#refresh').bind('click', function () {
             $('#table-pertemuan').DataTable().ajax.reload();
         });
 
         // Filter
         $('#cariFilter').click(function(){
           var bulan = $('#filterBulan').val();
           var bulanAkhir = $('#filterBulanAkhir').val();
           var factory = $('#filterFactory').val();
           var dept = $('#filterDept').val();
           console.log('Bulan : ', bulan+factory+dept);
 
           if(bulan != '' || bulanAkhir != ''){
             if(bulan != '' && bulanAkhir != ''){
               table.search(bulan+'^'+bulanAkhir+'^'+factory+'^'+dept).draw();
               console.log('lolos');
             }
             else{
               // console.log('tidak lolos');
             }
           }
           else{
             table.search(bulan+'^'+bulanAkhir+'^'+factory+'^'+dept).draw();
           }
 
         });
 
         $('#resetFilter').click(function(){
           table.search('').draw();
           $('#filterBulan').val('');
           $('#filterBulanAkhir').val('');
           $('#filterFactory').val('');
           $('#filterDept').val('');
         });

				 //  Alert Delete
				$('body').on('click', '#buttonDeletePertemuan', function(){
					var id = $(this).data('id');
					console.log('Data : '+id);

					var bulan = $('#filterBulan').val();
					var bulanAkhir = $('#filterBulanAkhir').val();
					var factory = $('#filterFactory').val();
					var dept = $('#filterDept').val();
					console.log('Bulan : ', bulan+factory+dept);

					Swal.fire({
            'title': 'Delete ?',
            'icon': 'question',
            'timer': 2000,
            'timerProgressBar': true,
            'showDenyButton': true,
            'confirmButtonText': `Delete`,
            'denyButtonText': `Don't Delete`,
          }).then((result) => {
              if (result.isConfirmed) {

                  var id_pertemuan = $(this).data("id");

                  console.log('Id : ', id_pertemuan);
									$.ajax({
                      url: "<?php echo base_url();?>Komite/delete_ss/"+id_pertemuan,
                      success: function (data) {
                          table.draw();
                          Swal.fire({
                            title: 'Deleted',
                            icon: 'success',
                            timer: 1000,
                            timerProgressBar: true,
                            position: 'bottom-end',
                            showConfirmButton: false,
                          });
                      },
                      error: function (data) {
                          console.log('Error:', data);
                      }
                  });
				
									if(bulan != '' || bulanAkhir != ''){
										if(bulan != '' && bulanAkhir != ''){
											table.search(bulan+'^'+bulanAkhir+'^'+factory+'^'+dept).draw();
											console.log('lolos');
										}
									}
									else{
										table.search(bulan+'^'+bulanAkhir+'^'+factory+'^'+dept).draw();
									}
            } else if (result.isDenied) {
              Swal.fire('Canceled', '', 'info')
            }
          });
				})
 
       });
 
     $(document).ready(function() {
 
       $('.departement').select2({
           // placeholder: " -- Choose NIK Sub Dept Head -- ",
           ajax: {
             url: 'Komite/data_department',
             dataType: 'json',
             delay: 250,
             data: function(params){
               return{
                 subdept : params.term
               };
             },
             processResults: function (data) {
               return {
                   results: $.map(data, function (item) {
                       return {
                           text: item.department_name,
                           id: item.department_name
                       }
                   })
               };
             },
             cache: true
           }
       });
 
     $(document).ready(function() {
         $('.factory').select2({
         // placeholder: " -- Choose NIK Sub Dept Head -- ",
         ajax: {
           url: 'Komite/data_factory',
           dataType: 'json',
           delay: 250,
           data: function(params){
             return{
               subdept : params.term
             };
           },
           processResults: function (data) {
             return {
                 results: $.map(data, function (item) {
                     return {
                         text: item.factory,
                         id: item.factory
                     }
                 })
             };
           },
           cache: true
         }
       });
     });
 
 
 });
 
     
     // function delete(id) {
     //     swal({
     //         console.log($id);
     //         title: "Apakah anda yakin?",
     //         text: "Data akan di hapus?",
     //         icon: "warning",
     //         buttons: true,
     //         dangerMode: true
     //         })
     //         .then((wildelete) => {
     //         if (wildelete) {
     //             $.ajax({
     //                 url:'<?php echo base_url();?>Komite/delete_ss?id='+id,
     //                 success:function(value){
     //                     if(value=='1'){
     //                         $("#alert_success").trigger("click", 'Hapus data Berhasil');                        
     //                         $('#refresh').trigger("click");
     //                     }else{
     //                         $("#alert_error").trigger("click", 'Hapus data gagal');
     //                     }
     //                 }
 
     //             });
                 
     //         }
     //     });
     //     return false;
     // }
 
    
 </script>
 