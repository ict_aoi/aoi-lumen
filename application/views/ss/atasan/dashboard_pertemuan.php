<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pertemuan Improvement </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Suggestion System (SS)</a></li>
              <li class="breadcrumb-item active">Daftar SS</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
            <a href = "<?= base_url('Atasan/dashboard_add_pertemuan'); ?>">
                    <button type="button" class="btn btn-outline-primary">Buat Pertemuan</button>
            </a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                    <input type = "hidden" value = "SS" id= "category" name= "category"/>
                    <div class="table-responsive">
                      <table id = "table-pertemuan" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%"  >
                        <thead>
                          <tr>
                          <th> No </th>
                          <th> Name </th>
                          <th> Judul</th>
                          <th> Tgl <br> Dibuat </th>
                          <th> Tgl <br> Perbaikan </th>
                          <th> Dept</th>
                          <th> Factory </th>
                          <th> App Sub<br> Dept.Head </th>
                          <th> App <br> Dept. Head </th>
                          <th> App <br> Komite </th>
                          <th> Detail </th>
                         
                          
                          </tr>
                        </thead>
                        <tbody>
                     
                        </tbody> 
                      </table>
                    </div>
                  </div>
            <!-- /.card-body -->
          </div>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>

  <!-- <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
  
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script> -->

	<link href="<?= base_url('assets/plugins/datatables/jquery.dataTables.css');?>" rel="stylesheet" />
  <script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.js');?>"></script>
	
  <script type="text/javascript">
     
     $(document).ready(function () {

        
        var table =$('#table-pertemuan').DataTable({
            "processing": true,
            "serverSide": true,
            // "order": [],
            "orderMulti"  : true,
            "ajax":{
            "url": "<?php echo base_url();?>Atasan/list_pertemuan",
            "dataType": "json",
            "type": "POST",
            "data":function(data) { 
                                        
            //    data.category = $('#category').val();
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                                    } },
        "columns": [
                { "data" : null, 'sortable' : false},
                { "data": "name" },
                { "data": "judul" },
                { "data": "date_created" },
                { "data": "meeting_date" },
                { "data": "department_name" },
                { "data": "factory" },
                { "data": "approve_subdept_head" },
                { "data": "approve_dept_head" },
                { "data": "approve_komite" },  
                { "data": "action",'sortable' : false },
            ],
            fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
        });
        $('#table-pertemuan_filter input').unbind();
        $('#table-pertemuan_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13 || $(this).val().length == 0 ) {
                table.search($(this).val()).draw();
            }
            // if ($(this).val().length == 0 || $(this).val().length >= 3) {
            //     table.search($(this).val()).draw();
            // }
        });
        $('#refresh').bind('click', function () {
            $('#table-pertemuan').DataTable().ajax.reload();
        });

    });
</script>
