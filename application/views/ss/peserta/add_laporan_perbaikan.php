<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Buat Pertemuan </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Suggestion System (SS)</a></li>
              <li class="breadcrumb-item active">Daftar SS</li>
              <li class="breadcrumb-item active">Detail SS</li>
              <li class="breadcrumb-item active">Tambah Laporan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
                      <div class="card-body">
                     
                      <?=$this->session->flashdata('notif')?>
                        <?php
                      if($laporan != NULL){
                        foreach ($laporan as $l){
                          
                        ?>
                           <form class="forms-sample" action="<?= base_url('Peserta/update_laporan_ss/'.$l->id_laporan); ?>" method="post" enctype="multipart/form-data">
                        <input type = "hidden" name = "id_laporan" value ="<?php echo $l->id_laporan ?>" />
                        <input type = "hidden" name = "id_pertemuan" value ="<?php echo $l->id_pertemuan ?>" />
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Aktivitas Perbaikan</label>
                            <div class="col-sm-9">
                              <textarea type="text" class="form-control" id="aktivitas_perbaikan"  name="aktivitas_perbaikan"  ><?php echo $l->aktivitas_perbaikan ?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Ilustrasi Produk (Before)</label>
                            <div class="input-group col-sm-9">
                            <input type="file" name="produk_bef">  <span class="input-group-append">
                               </span>
                           
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Ilustrasi Produk (After)</label>
                            <div class="input-group col-sm-9">
                            <input type="file" name="produk_aft">  <span class="input-group-append">
                                   </span>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Biaya Improvement</label>
                            <div class="col-sm-9">
                              <textarea type="text" class="form-control" id="biaya_improvement"  name="biaya_improvement"><?php echo $l->biaya_imp ?></textarea>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Perhitungan Benefit</label>
                            <div class="col-sm-9">
                              <textarea type="text" class="form-control" id="perhitungan_benefit"  name="perhitungan_benefit"  ><?php echo $l->perhitungan_benefit ?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Total Benefit</label>
                            <div class="col-sm-9">
                              <input type="number" class="form-control" id="total_benefit"  name="total_benefit"  value = "<?php echo $l->total_benefit ?>">
                            </div>
                          </div>
                          

                          <table class="table table-striped">
                      <thead>
                        <tr>
                          <th > Faktor </th>
                          <th> Before </th>
                          <th> After </th>
                     

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        <td >Quality</td>
                        <td ><input type = "text" class="form-control" name = "quality_before" value = "<?php echo $l->quality_before ?>"/></td>
                        <td ><input type = "text" class="form-control" name = "quality_after" value = "<?php echo $l->quality_after ?>"/></td>
                
                        </tr>
                        <tr>
                        <td >Cost</td>
                        <td> <input type = "text" class="form-control" name = "cost_before" value = "<?php echo $l->cost_before ?>"/></td>
                        <td ><input type = "text" class="form-control" name = "cost_after" value = "<?php echo $l->cost_after ?>"/></td>
                
                        </tr>
                        <tr>
                        <td >Delivery</td>
                        <td ><input type = "text" class="form-control" name = "delivery_before" value = "<?php echo $l->delivery_before ?>"/></td>
                        <td ><input type = "text" class="form-control" name = "delivery_after" value = "<?php echo $l->delivery_after ?>"/></td>
                
                        </tr>
                        <tr>
                        <td >Safety</td>
                        <td ><input type = "text" class="form-control" name = "safety_before" value = "<?php echo $l->safety_before ?>"/></td>
                        <td ><input type = "text" class="form-control" name = "safety_after" value = "<?php echo $l->safety_after ?>"/></td>
                
                        </tr>
                        <tr>
                        <td >Moral</td>
                        <td ><input type = "text" class="form-control" name = "moral_before" value = "<?php echo $l->moral_before ?>"/></td>
                        <td ><input type = "text" class="form-control" name = "moral_after" value = "<?php echo $l->moral_after ?>"/></td>
                
                        </tr>
                        <tr>
                        <td >Productivity</td>
                        <td ><input type = "text" class="form-control" name = "productivity_before" value = "<?php echo $l->productivity_before ?>"/></td>
                        <td ><input type = "text" class="form-control" name = "productivity_after" value = "<?php echo $l->productivity_after ?>"/></td>
                
                        </tr>
                        <tr>
                        <td >Environtment</td>
                        <td ><input type = "text" class="form-control" name = "environtment_before" value = "<?php echo $l->environtment_before ?>"/></td>
                        <td><input type = "text" class="form-control" name = "environtment_after" value = "<?php echo $l->environtment_after ?>"/></td>
                
                        </tr>
                       
                     
                      </tbody> 
                    </table>
                   
            

                        
                          <button type="submit" class="btn btn-success mr-2">Submit</button>
                          <button class="btn btn-light">Cancel</button>
                    
                        </form>
                        <?php }
                      } else{
                        ?>
                           <form class="forms-sample" action="<?= base_url('Peserta/tambah_laporan_ss/'.$id_pertemuan); ?>" method="post" enctype="multipart/form-data">
                        <input type = "hidden" value ="<?php echo $id_pertemuan ?>" />
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Aktivitas Perbaikan</label>
                            <div class="col-sm-9">
                              <textarea type="text" class="form-control" id="aktivitas_perbaikan"  name="aktivitas_perbaikan"  ></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Ilustrasi Produk (Before)</label>
                            <div class="input-group col-sm-9">
                            <input type="file" name="produk_bef">  <span class="input-group-append">
                               </span>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Ilustrasi Produk (After)</label>
                            <div class="input-group col-sm-9">
                            <input type="file" name="produk_aft">  <span class="input-group-append">
                                   </span>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Biaya Improvement</label>
                            <div class="col-sm-9">
                              <textarea type="text" class="form-control" id="biaya_improvement"  name="biaya_improvement"  ></textarea>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Perhitungan Benefit</label>
                            <div class="col-sm-9">
                              <textarea type="text" class="form-control" id="perhitungan_benefit"  name="perhitungan_benefit"  ></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Total Benefit</label>
                            <div class="col-sm-9">
                              <input type="number" class="form-control" id="total_benefit"  name="total_benefit"  >
                            </div>
                          </div>
                          

                          <table class="table table-striped">
                      <thead>
                        <tr>
                          <th > Faktor </th>
                          <th> Before </th>
                          <th> After </th>
                     

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        <td >Quality</td>
                        <td ><input type = "text" class="form-control" name = "quality_before"/></td>
                        <td ><input type = "text" class="form-control" name = "quality_after"/></td>
                
                        </tr>
                        <tr>
                        <td >Cost</td>
                        <td> <input type = "text" class="form-control" name = "cost_before"/></td>
                        <td ><input type = "text" class="form-control" name = "cost_after"/></td>
                
                        </tr>
                        <tr>
                        <td >Delivery</td>
                        <td ><input type = "text" class="form-control" name = "delivery_before"/></td>
                        <td ><input type = "text" class="form-control" name = "delivery_after"/></td>
                
                        </tr>
                        <tr>
                        <td >Safety</td>
                        <td ><input type = "text" class="form-control" name = "safety_before"/></td>
                        <td ><input type = "text" class="form-control" name = "safety_after"/></td>
                
                        </tr>
                        <tr>
                        <td >Moral</td>
                        <td ><input type = "text" class="form-control" name = "moral_before"/></td>
                        <td ><input type = "text" class="form-control" name = "moral_after"/></td>
                
                        </tr>
                        <tr>
                        <td >Productivity</td>
                        <td ><input type = "text" class="form-control" name = "productivity_before"/></td>
                        <td ><input type = "text" class="form-control" name = "productivity_after"/></td>
                
                        </tr>
                        <tr>
                        <td >Environtment</td>
                        <td ><input type = "text" class="form-control" name = "environtment_before"/></td>
                        <td><input type = "text" class="form-control" name = "environtment_after"/></td>
                
                        </tr>
                       
                     
                      </tbody> 
                    </table>
                   
            

                        
                          <button type="submit" class="btn btn-success mr-2">Submit</button>
                          <button class="btn btn-light">Cancel</button>
                    
                        </form>
                        <?php
                      }
                      ?>
                        
                       
                      </div>
                    
                    </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>

