<?php
    $this->load->view('partials/header');
    
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Buat Pertemuan </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('Login/dashboard');?>">Suggestion System (SS)</a></li>
              <li class="breadcrumb-item active">Daftar SS</li>
              <li class="breadcrumb-item active">Buat SS</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
                      <div class="card-body">
                     
                        <?=$this->session->flashdata('notif')?>
                        <form class="forms-sample" action="<?= base_url('Peserta/tambah_pertemuan_ss'); ?>" method="post">
                            
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">NIK</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="nik"  name="nik" readonly value="<?php echo $this->session->userdata('nik') ?>" >
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="name"  name="name" readonly value="<?php echo $this->session->userdata('name') ?>" >
                            </div>
                          </div>
                         
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Departement</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="department"  name="department" readonly value="<?php echo $this->session->userdata('department_name') ?>" >
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label" >Category</label>
                           
                            <div class="col-md-6">
                          <div class="form-group">
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name = "category_percategory[]" value = "Morale"> Morale </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input"  name = "category_percategory[]" value = "Cost" checked > Cost </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input"  name = "category_percategory[]" value = "Safety"> Safety/5R </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input"  name = "category_percategory[]" value = "Productivity" > Productivity </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name = "category_percategory[]" value = "Lingkungan" > Lingkungan </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name = "category_percategory[]" value = "Delivery" > Delivery </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name = "category_percategory[]" value = "Quality" > Quality </label>
                            </div>
                            <div class="form-check form-check-flat">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name = "category_percategory[]" value = "Other" > Other </label>
                            </div>
                          </div>
                        </div>
                        
                          </div>
                          <div class="form-group row">
                          <label for="exampleTextarea1"  class="col-sm-3 col-form-label">Judul / Tema</label>
                            <div class="col-sm-9">
                            <textarea class="form-control" id="judul" name="judul" rows="2" placeholder="Judul diisi sesuai judul SS anda" required></textarea>
                            </div>
                          </div>
                         
                          <div class="form-group row">
                          <label for="exampleTextarea1"  class="col-sm-3 col-form-label">Uraian Masalah</label>
                            <div class="col-sm-9">
                            <textarea class="form-control" id="uraian_masalah" name = "uraian_masalah" rows="2" placeholder = "Uraian masalah sesuai dengan masalah anda " required></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                          <label for="exampleTextarea1"  class="col-sm-3 col-form-label">Keadaan Seharusnya</label>
                            <div class="col-sm-9">
                            <textarea class="form-control" id="keadaan_seharusnya" name = "keadaan_seharusnya" rows="2" placeholder = "Keadaan seharusnya " required></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                          <label for="exampleTextarea1"  class="col-sm-3 col-form-label">Usulan Perbaikan</label>
                            <div class="col-sm-9">
                            <textarea class="form-control" id="usulan_perbaikan" name="usulan_perbaikan" rows="2" placeholder = "Usulan perbaikan sesuai dengan anda usulkan" required></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                          <label for="exampleTextarea1"  class="col-sm-3 col-form-label">Biaya Kerugian</label>
                            <div class="col-sm-9">
                            <textarea class="form-control" id="biaya_kerugian" name="biaya_kerugian" rows="2" required placeholder = "Biaya kerugian"></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label" required>Tanggal Perbaikan</label>
                            <div class="col-sm-9">
                              <input type="date" class="form-control" id="meeting_date"  name="meeting_date">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Waktu Perbaikan</label>
                            <div class="col-sm-9">
                              <input type="time" class="form-control" id="meeting_time" name ="meeting_time" >
                            </div>
                          </div>
                          
                      
                          <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Lokasi Perbaikan</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="location" name ="location" required >
                            </div>
                          </div>
                       
                          <div class="form-group row">
                        
                              <label for="exampleInputPassword2" class="col-sm-3 col-form-label">NIK Supervisor</label>
                              <div class="col-sm-9">
                              <!-- <select id="nik_spv" name="nik_spv" class="form-control">
                                  <option value=""></option>
                                  <?php
                                foreach ($spv->result() as $baris) {

                                  echo "<option value='".$baris->nik."'>".$baris->nik.' - '.$baris->name."</option>";
                                }
                              ?>
                              
                              </select> -->
                              <select class="form-control spv" name="spv" id = "spv" >
                              </select>
                              </div>
                          </div>

                          <div class="form-group row">
                        
                              <label for="exampleInputPassword2" class="col-sm-3 col-form-label">NIK Sub Department Head</label>
                              <div class="col-sm-9">
                           
                              <select class="form-control subdept" name="subdept" id = "subdept" class="form-control"  >
                              </select>
                              </div>
                          </div>
                          
                          <div class="form-group row">
                        
                              <label for="exampleInputPassword2" class="col-sm-3 col-form-label">NIK Department Head</label>
                              <div class="col-sm-9">
                              <select class="form-control depthead" name="depthead" id = "depthead" class="form-control"  >
                           
                              </div>
                          </div>
                        
                        
                          <div class="form-group row">
                              <input  class="form-control" id="category" name ="category" value = "<?php echo $category ?>" type = "hidden" >
                            <div class="col-md-6">
                         
                          </div>
                          </div>
                          
                       
                      
                    
                          
                          
                     <!--   <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Foto Organisasi Team</label>
                            <input type="file" name="img[]" class="file-upload-default">
                            <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                            </span>
                            </div>
                        </div> -->

                        
                          <button type="submit" class="btn btn-success mr-2">Submit</button>
                          <button class="btn btn-light">Cancel</button>
                  
                        </form>
                        
                       
                      </div>
                    
                    </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


  <!-- Main Footer -->
  <?php
  $this->load->view('partials/footer');

  ?>


  </div>

<link href="<?= base_url('assets/plugins/select2/css/select2.min.css');?>" rel="stylesheet" />
<link href="<?= base_url('assets/plugins/select2/css/select2.css');?>" rel="stylesheet" />
<link href="<?= base_url('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');?>" rel="stylesheet" />
<script src="<?= base_url('assets/plugins/select2/js/select2.min.js');?>"></script>
<script>

  $(document).ready(function() {
    $('.subdept').select2({
        // placeholder: " -- Choose NIK Sub Dept Head -- ",
        ajax: {
          url: 'data_subdept',
          dataType: 'json',
          delay: 250,
          data: function(params){
            return{
              subdept : params.term
            };
          },
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nik + ' - ' + item.name,
                        id: item.nik
                    }
                })
            };
          },
          cache: true
        }
    });
});
$(document).ready(function() {
    $('.spv').select2({
        // placeholder: " -- Choose NIK Sub Dept Head -- ",
        ajax: {
          url: 'data_spv',
          dataType: 'json',
          delay: 250,
          data: function(params){
            return{
              spv : params.term
            };
          },
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nik + ' - ' + item.name,
                        id: item.nik
                    }
                })
            };
          },
          cache: true
        }
    });
});
$(document).ready(function() {
    $('.depthead').select2({
        // placeholder: " -- Choose NIK Sub Dept Head -- ",
        ajax: {
          url: 'data_depthead',
          dataType: 'json',
          delay: 250,
          data: function(params){
            return{
              depthead : params.term
            };
          },
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nik + ' - ' + item.name,
                        id: item.nik
                    }
                })
            };
          },
          cache: true
        }
    });
});
  </script> 

